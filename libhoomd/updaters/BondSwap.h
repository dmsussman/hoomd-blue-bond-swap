/*
Highly Optimized Object-oriented Many-particle Dynamics -- Blue Edition
(HOOMD-blue) Open Source Software License Copyright 2009-2015 The Regents of
the University of Michigan All rights reserved.

HOOMD-blue may contain modifications ("Contributions") provided, and to which
copyright is held, by various Contributors who have granted The Regents of the
University of Michigan the right to modify and/or distribute such Contributions.

You may redistribute, use, and create derivate works of HOOMD-blue, in source
and binary forms, provided you abide by the following conditions:

* Redistributions of source code must retain the above copyright notice, this
list of conditions, and the following disclaimer both in the code and
prominently in any materials provided with the distribution.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions, and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* All publications and presentations based on HOOMD-blue, including any reports
or published results obtained, in whole or in part, with HOOMD-blue, will
acknowledge its use according to the terms posted at the time of submission on:
http://codeblue.umich.edu/hoomd-blue/citations.html

* Any electronic documents citing HOOMD-Blue will link to the HOOMD-Blue website:
http://codeblue.umich.edu/hoomd-blue/

* Apart from the above required attributions, neither the name of the copyright
holder nor the names of HOOMD-blue's contributors may be used to endorse or
promote products derived from this software without specific prior written
permission.

Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND/OR ANY
WARRANTIES THAT THIS SOFTWARE IS FREE OF INFRINGEMENT ARE DISCLAIMED.

IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


/*! \file BondSwap.h
    \brief Declares an updater that performs MC bond swaps
*/


#include <Updater.h>
#include <ForceCompute.h>
#include <NeighborListBinned.h>
#include <saruprng.h>
#include <boost/shared_ptr.hpp>
#include <boost/python/stl_iterator.hpp>
#include <boost/python/numeric.hpp>

#ifndef _BOND_SWAP_H_
#define _BOND_SWAP_H_

//! Class for performing bond swaps according to a Monte Carlo routine
/*!
    <b>Overview:</b>
    This class implements a Monte Carlo procedure for swapping bonds between polymers. It
    is intended to be used to speed up the equilibration of long, heavily entangled linear
    chains, but it is not restricted to this use case. Essentially, the BondSwap updater
    accesses a neighborlist and looks for sets of nearby particles that satisfy a swap-suitability
    criteria. The default behavior uses a Boltzmann acceptance criteria to try to graft
    the end of chain 1 onto a nearby chain 2,  and vice versa, while preserving the initial
    distribution of chain lengths. However, the user can supply an alternate set of molecular
    information (see details below) to produce very different behavior.
    With the default behavior the internal distance statistics of polymer chains should
    converge to equilibrium values in many fewer timesteps than by brute forcing running
    the dynamics.

    <b>Implementation notes:</b>
    This updater relies on the notion of position along the chain of a given monomer,
    although more generally the object is just an unsigned integer assigned to each
    particle tag, to determine potential bond swaps. For the purposes of documentation
    we will denote the chain position label given to tag \f$i\f$ as \f$P(i)\f$. During the updater' s
    compute step, a fraction of particles with tag \f$i\f$ look through their neighbor lists
    for particles with tag \f$j\f$ such that \f$P(i)==P(j)\f$. If such a match is found, the updater
    loops over all particles bonded to particles \f$i, j\f$, looking for bonded neighbors \f$i_2, j_2\f$
    such that \f$P(i_2)==P(j_2)\f$. A final condition on the suitability for a proposed swap is that
    the distance between \f$(i,j_2)\f$ and \f$(j,i_2)\f$ are within the user-specified range. Playing with
    the softness of bonds and the range considered may considerably speed up equilibration.

    In a given timestep, if the updater finds a set of four particles that satisfy all of these
    criteria, the energetic cost of replacing the \f$(i,i_2)\f$ and \f$(j,j_2)\f$ bonds with \f$(i,j_2)\f$ and
    \f$(j,i_2)\f$ bonds is computed by looping over all forceComputes that have been associated with
    this updater and calling the computeEnergySet  methods (which were written for this updater).
    The bond swap is then accepted according to a Boltzmann criteria: if the energy decreases the swap
    is accepted, and if it increase the swap is accepted with probability \f$\exp(-E_d/kT)\f$,
    where \f$E_d\f$ is the energy difference, \f$k\f$ is Boltzmann's constant, and \f$T\f$ is the
    temperature of the updater. Note that the updater's temperature is specified to be
    independent of the temperature of the simulation itself.

    Note: the current behavior of the updater assumes that particles that share a bond are in each
    other's exclusion list (this assumption is made in the getEnergyPairBond helper function). This
    could be changed if desired.

    When the system is composed entirely of linear chains that appear
    in contiguous blocks of particle tags the default constructor will give chain
    positions \f$P(i)\f$ that are the distance (plus one) of a given monomer from the chain
    end. I.e., if tags 0 - 6 refer to one chain and tags 7 - 16 refer to a second chain
    the default \f$P(i)\f$ values for \f$0 \leq i \leq 15\f$ would be:
    1,2,3,4,3,2,1,1,2,3,4,5,5,4,3,2,1. In any other tag initialization condition the
    user should specify the chain positions by hand in order
    to recover whatever behavior is expected. For instance, specifying a single \f$P\f$
    value for every particle tag will allow chains to freely swap, growing and shrinking
    in length and hence changing the chain length dispersity. More intricate rules for
    what swaps should be allowed can be implemented by specifying the \f$P(i)\f$ values
    and paying attention to the above rules for how the updater functions.

    <b> Safety mode </b>
    This updater was originally written with the equilibration of simple linear chains in mind,
    and several optimizations were written for this use case. As mentioned above, the default labeling
    of \f$P(i)\f$ assumes linear chains where swaps can occur from either end of the chains and enforces
    an unchanging distribution of chain lengths. In this use case the number of exclusions per particle
    does not change (nor does the number of bonds, angles, or dihedrals)... the exclusions may be swapped
    between particles, but their total number and the number per particle is fixed. The same is true of
    the number of bonds, angles, and dihedrals. This allows for computationally efficient bond swaps,
    as in the tables of exclusions (or bonds, or angles, etc.) appropriate particle tags can be scanned
    for and altered rather than invoking expensive rebuilds of various tables.

    However, it is possible to set conditions which violate some of the assumptions under which these
    optimizations procede. For instance, if \f$P(i)=1\f$ for all \a i then chains will swap bonds
    without maintaining the initial distribution of chain lengths. Thus, e.g., a swap could be made that
    takes two long chains to a very long  chain and a dimer, which would change the number of
    dihedrals in the system.

    NOTE: While a generic implementation of angles is included, using dihedrals in conditions where
    the total number of dihedrals can change during a swap is not currently supported. The code will
    exit out if it detects that the use case is incorrect.

    The flag m_safety_mode, which can be set from the Python code,
    triggers a change so that on every timestep in which a bond swap occurs both the auxiliary tables used in
    the updater and the exclusions used throughout the program are completely rebuilt. Particularly for large
    systems this can be quite costly, but can be used to enforce correct behavior more generally than the
    default operation of the updater. Default operation of the code, i.e. using the automatically-generated
    values of \f$P(i)\f$, does not require changing this flag.
*/
class BondSwap : public Updater
    {
    public:
        //! Constructor
        BondSwap(boost::shared_ptr<SystemDefinition> sysdef,
                boost::shared_ptr<NeighborList> nlist,
                Scalar fraction,
                Scalar distSquared,
                Scalar Tnew,
                int seed,
                int period);

        //!Destructor. Outputs MC statistics at notice level 3
        virtual ~BondSwap();

        //! The main routine. Look for and perform bond swaps.
        virtual void update(unsigned int timestep);

        //!Add a ForceCompute to the list. Used to initialize updater from python
        virtual void addForceCompute(boost::shared_ptr<ForceCompute> fc);

        //! Tell the BondSwap updater to enter "safety mode" handling of topology and exclusions. Default is false
        void setSafetyMode(bool safety);

        //! Tell the BondSwap updater to update angle exclusions after a swap. Default is false
        void setAngleExclusions(bool exclude){m_excl_angle = exclude;
                                             if(m_excl_angle) m_nlist->addExclusionsFromAngles();};

        //! Tell the BondSwap updater to update dihedral exclusions after a swap. Default is false
        void setDihedralExclusions(bool exclude){m_excl_dihedral = exclude;
                                            if(m_excl_dihedral) m_nlist->addExclusionsFromDihedrals();};

        //! Change the internal temperature of the BondSwap updater
        void setMCTemperature(Scalar tNew){m_Tnew = tNew; m_exec_conf->msg->notice(5)
                        << "BondSwap internal temperature changed to " << m_Tnew << std::endl;};

        //! Initialization command to build all auxiliary tables
        void buildTables();

        //! print MC statistics to screen
        void printBondSwapStats();

        //! pass a python list of unsigned integers, set m_chain_position array by it
        void setChainPositions(boost::python::list positions);

    protected:
        boost::shared_ptr<NeighborList> m_nlist;//!< neighbor list to use
        Scalar m_fraction;          //!< fraction of particles to check for swaps
        Scalar m_distSquared;       //!< maximum squared distance at which to consider particle swaps
        Scalar m_Tnew;              //!< internal updater temperature to be used for the MC acceptance criteria
        int m_seed;                 //!< seed for the saru RNG (along with the timestep in the two-see construction)
        int m_period;               //!< update period (deprecate this?)
        GPUArray<int> m_mc_stats;   //!< array holding attempt and acceptance information for the MC moves

        //! New O(N) data structure holding information about what particle swaps are allowed
        GPUArray<unsigned int> m_chain_position;

        //! vector of ForceComputes over which to evaluate the energy cost of proposed swaps
        std::vector< boost::shared_ptr<ForceCompute> > m_forces;
        std::string pairStr, bondStr,angleStr,dihedralStr; //!< strings to help determine force compute types

        bool m_safety_mode;         //!< flag to enable "safety mode" handling of exclusions and topology

        bool m_check_angle;         //!< flag for whether there are angles to consider
        bool m_check_dihedral;      //!< flag for whether there are dihedrals to consider

        bool m_excl_angle;         //!< flag for whether angles should be excluded
        bool m_excl_dihedral;      //!< flag for whether dihedrals should be excluded


        //! auxilliary lists to distinguish bonds, angles, dihedrals for efficient cpu eval... currently disfavored
        //std::vector< std::vector<unsigned int> > m_bonded;
        std::vector< std::vector<unsigned int> > m_bond_table;
        std::vector<std::vector< std::vector<unsigned int> > > m_angle_table;
        std::vector<std::vector< std::vector<unsigned int> > > m_dihedral_table;

        //! a pair of class variables to play with for assorted testing purposes
        //! If anyone other than DMS is reading this comment, DO NOT trust the results of this code,
        //! as it is probably not functioning as intended!
        int test1, test2;

        //! helper function takes the log name of a forceCompute and returns a pair, bond, angle, or dihedral identifying number
        unsigned int forceComputeType(const std::string& str)
            {
               if(str.compare(0,pairStr.length(),pairStr)==0)
                   return 1;
               if(str.compare(0,bondStr.length(),bondStr)==0)
                   return 2;
               if(str.compare(0,angleStr.length(),angleStr)==0)
                   return 3;
               if(str.compare(0,dihedralStr.length(),dihedralStr)==0)
                   return 4;
                return 0;
            };

        //! Looks through the system's bond list for a bond with tags (bi1,bi2)
        //! in either order and replaces those tags with the pair (bf1,bf2)
        void changeBondByTag(unsigned int bi1, unsigned int bi2, unsigned int bf1, unsigned int bf2);

        //! Look through angles and replace the initial with the final tags
        void changeAngleByTag(unsigned int ai1, unsigned int ai2, unsigned int ai3,
                              unsigned int af1, unsigned int af2, unsigned int af3);

        //! Look through dihedrals and replace the initial with the final tags
        void changeDihedralByTag(unsigned int di1, unsigned int di2, unsigned int di3, unsigned int di4,
                                 unsigned int df1, unsigned int df2, unsigned int df3, unsigned int df4);

        //!Computes the total potential energy of all forceComputes that have been passed to the BondSwap updater
        Scalar getEnergyStep(bool forceUpdate, unsigned int timestep);

        //! Assuming that the BondSwap updater already has ForceComputes, this computes the energetic cost
        //! of making a bond (pi1,pi2) and (pj1,pj2) according to whatever bond type bond indices bond_ind1
        //! and bond_ind2 are. It also computes the pair interaction costs of (pi1,pj2) and (pj1,pi2).
        Scalar getEnergyPairBond(unsigned int pi1, unsigned int pi2, unsigned int pj1, unsigned int pj2,
                                 unsigned int bond_ind1, unsigned int bond_ind2);

        //! Assuming that the BondSwap updater already has ForceComputes,
        //! this computes the energetic cost of the angle (pi1,pi2,pi3, angle_index)
        Scalar getEnergyAngle(unsigned int pi1, unsigned int pi2, unsigned int pi3, unsigned int angle_index);

        //! Assuming that the BondSwap updater already has ForceComputes,
        //! this computes the energetic cost of the dihedral (pi1,pi2,pi3,pi4, dihedral_index)
        Scalar getEnergyDihedral(unsigned int pi1, unsigned int pi2,
                                 unsigned int pi3, unsigned int pi4, unsigned int dihedral_index);

        //! Given a proposed swap candidate, evaluate whether it is accepted by computing the energetic cost
        bool attemptSwap(std::vector<int> &quad,int timestep,
                        std::vector< std::vector<unsigned int> > &bond_table,
                        std::vector<std::vector<std::vector<unsigned int> > > &angle_table,
                        std::vector<std::vector<std::vector<unsigned int> > > &dihedral_table,
                        std::vector<std::vector<std::vector<unsigned int> > > &angle_change,
                        std::vector<std::vector<std::vector<unsigned int> > > &dihedral_change);

        //! Search nearby particles for potential swap proposals (pair and bond distance criteria)
        virtual void findPotentialSwaps(std::vector<std::vector<int> > &quads, int timestep);

        //! Find angles that would be changed by a bond swap
        void findNearbyAngles(unsigned int iTag, unsigned int jTag,
                              unsigned int swap_tag_i, unsigned int swap_tag_j,
                              std::vector< std::vector<unsigned int> > &initial_angles,
                              std::vector< std::vector<unsigned int> > &final_angles,
                              std::vector< std::vector<unsigned int> > &bond_table,
                              std::vector<std::vector<std::vector<unsigned int> > > &angle_table);

        void findNearbyDihedrals(unsigned int iTag, unsigned int jTag,
                              unsigned int swap_tag_i, unsigned int swap_tag_j,
                              std::vector< std::vector<unsigned int> > &initial_dihedrals,
                              std::vector< std::vector<unsigned int> > &final_dihedrals,
                              std::vector< std::vector<unsigned int> > &bond_table,
                              std::vector<std::vector<std::vector<unsigned int> > > &dihedral_table);

        //! Helper function to reformat info about angles in the system
        void buildBondTable(std::vector<std::vector<unsigned int> > &bonded);

        //! Helper function to reformat info about angles in the system
        void buildAngleTable(std::vector<std::vector<std::vector<unsigned int> > > &angle_table);

        //! Helper function to reformat info about dihedrals in the system
        void buildDihedralTable(std::vector<std::vector<std::vector<unsigned int> > > &dihedral_table);

    };

//! Export the ExampleUpdater class to python
void export_BondSwap();



#endif // _BOND_SWAP_H_

