/*
Highly Optimized Object-oriented Many-particle Dynamics -- Blue Edition
(HOOMD-blue) Open Source Software License Copyright 2009-2015 The Regents of
the University of Michigan All rights reserved.

HOOMD-blue may contain modifications ("Contributions") provided, and to which
copyright is held, by various Contributors who have granted The Regents of the
University of Michigan the right to modify and/or distribute such Contributions.

You may redistribute, use, and create derivate works of HOOMD-blue, in source
and binary forms, provided you abide by the following conditions:

* Redistributions of source code must retain the above copyright notice, this
list of conditions, and the following disclaimer both in the code and
prominently in any materials provided with the distribution.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions, and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* All publications and presentations based on HOOMD-blue, including any reports
or published results obtained, in whole or in part, with HOOMD-blue, will
acknowledge its use according to the terms posted at the time of submission on:
http://codeblue.umich.edu/hoomd-blue/citations.html

* Any electronic documents citing HOOMD-Blue will link to the HOOMD-Blue website:
http://codeblue.umich.edu/hoomd-blue/

* Apart from the above required attributions, neither the name of the copyright
holder nor the names of HOOMD-blue's contributors may be used to endorse or
promote products derived from this software without specific prior written
permission.

Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND/OR ANY
WARRANTIES THAT THIS SOFTWARE IS FREE OF INFRINGEMENT ARE DISCLAIMED.

IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifdef ENABLE_MPI
#include "Communicator.h"
#endif

#include <iostream>
using namespace std;

#include "BondSwap.h"
#include <boost/python.hpp>
using namespace boost::python;


/*! \file BondSwap.cc
    \brief Defines the BondSwap class
*/


/*! \param sysdef System from which to compute
    \param nlist NeighborList to use for proposing swap candidates
    \param fraction Fraction of particles to check for swaps
    \param distSquared Maximum squared distance between swap candidates
    \param Tnew Temperature to use for Monte Carlo acceptance
    \param seed Seed to use (along with the timestep) for saru RNG
    \param period Period at which updater will run
*/
BondSwap::BondSwap(boost::shared_ptr<SystemDefinition> sysdef,
                boost::shared_ptr<NeighborList> nlist,
                Scalar fraction,
                Scalar distSquared,
                Scalar Tnew,
                int seed,
                int period)
    : Updater(sysdef), m_nlist(nlist),m_fraction(fraction),m_distSquared(distSquared),m_Tnew(Tnew),
    m_seed(seed),m_period(period),m_mc_stats(2,m_exec_conf),m_chain_position(1,m_exec_conf),
    pairStr("pair"),bondStr("bond"),angleStr("angle"),dihedralStr("dihedral")
    {
#ifdef ENABLE_MPI
    if (m_pdata->getDomainDecomposition())
        {
        m_exec_conf->msg->error() << "MPI CURRENTLY NOT SUPPORTED IN BONDSWAP UPDATER"  <<std::endl;
        throw runtime_error("BondSwap::BondSwap does not support MPI");
        }
#endif
    //initialization
    assert(m_nlist);
    m_exec_conf->msg->notice(5) << "Constructing BondSwap updater; f="<<m_fraction
                                <<" d="<<m_distSquared << " T_b=" <<m_Tnew << " seed="<<m_seed << endl;

    //by default, DISABLE m_safety mode
    m_safety_mode = false;
    //by default, no angles or dihedrals. These flags are automatically changed by addForceCompute
    m_check_angle = false;
    m_check_dihedral = false;

    //by default don't exclude 1-3 and 1-4 angle and dihedrals
    m_excl_angle = false;
    m_excl_dihedral = false;

    ArrayHandle<int> h_mc_stats(m_mc_stats,access_location::host, access_mode::overwrite);
    h_mc_stats.data[0]=0;
    h_mc_stats.data[1]=0;


    //make a new data structure for chain position
    //for now construct it from the bond data itself using swapType 2 convention
    m_chain_position.resize(m_pdata->getNGlobal());
    ArrayHandle<unsigned int> h_chain_position(m_chain_position,access_location::host, access_mode::overwrite);
    unsigned int local_rank = m_exec_conf->getRank();

    boost::shared_ptr<BondData> initialBonds = m_sysdef->getBondData();
    BondData::Snapshot snapshot(initialBonds->getNGlobal());
    initialBonds->takeSnapshot(snapshot);

    //broadcast global bond list
    std::vector<BondData::members_t> bonds;

#ifdef ENABLE_MPI
    if (m_pdata->getDomainDecomposition())
        {
        if (m_exec_conf->getRank()==0)
            bonds=snapshot.groups;
        bcast(bonds,0,m_exec_conf->getMPICommunicator());
        }
    else
#endif
        {
        bonds=snapshot.groups;
        };


    unsigned int size = bonds.size();
    std::vector<unsigned int> chain;
    for (unsigned int ii = 0; ii < size; ++ii)
        {
        const  BondData::members_t& bond = bonds[ii];
        if((chain.size()>0 && bond.tag[0] != chain.back())||ii+1 == size)
        {
            if(ii+1 ==size)
                {
                    chain.push_back(bond.tag[1]);
                };
            int chainSize = chain.size();
            for (int mm = 0; mm < chainSize; ++mm)
                {
                if (mm+1 <= 0.5*(Scalar)(chainSize)) h_chain_position.data[chain[mm]] = mm+1;
                if (mm+1 >  0.5*(Scalar)(chainSize)) h_chain_position.data[chain[mm]] = chainSize - mm;
                };
           chain.resize(0);
        };
        if(chain.size() == 0 )
            {
            chain.push_back(bond.tag[0]);
            chain.push_back(bond.tag[1]);
            continue;
            };
        if(bond.tag[0] == chain.back())
            {
            chain.push_back(bond.tag[1]);
            continue;
            };
        };

    // initialization also requires the auxiliary bond, angle, and dihedral tables...
    // at the moment this is done when the python interface calls buildTables
    m_exec_conf->msg->notice(5) << "Finished initializing bond swapper on local rank "
                                << local_rank << " with size " << size  << endl;

    };

BondSwap::~BondSwap()
    {
    m_exec_conf->msg->notice(5) << "Destroying BondSwap updater" << endl;


    ArrayHandle<int> h_mc_stats(m_mc_stats,access_location::host, access_mode::readwrite);
    if(h_mc_stats.data[0]>0)
        {
        Scalar frac = (Scalar)(h_mc_stats.data[1])/(Scalar)(h_mc_stats.data[0]);
        m_exec_conf->msg->notice(3) << std::endl << "--------------------"<<std::endl<<"Bond Swapping statistics: "
                                <<std::endl << "--------------------"  << std::endl
                                << "Attempted: " << h_mc_stats.data[0] << "\t Accepted: " << h_mc_stats.data[1]
                                << std::endl << "Fractional acceptance: " << frac << std::endl
                                <<std::endl << "--------------------" << std::endl;
        };
    }


/*! \param fc ForceCompute to add to updater to check energy changes from bond swaps
*/
void BondSwap::addForceCompute(boost::shared_ptr<ForceCompute> fc)
    {

    assert(fc);
    m_forces.push_back(fc);
    std::vector< std::string > f_log_name;
    f_log_name = fc->getProvidedLogQuantities();
    for (unsigned int name = 0; name < f_log_name.size(); ++name)
        {
        unsigned int ftype = forceComputeType(f_log_name[name]);
        if (ftype ==3)
            {
            m_check_angle = true;
            buildAngleTable(m_angle_table);
            }
        if (ftype ==4)
            {
            m_check_dihedral = true;
            buildDihedralTable(m_dihedral_table);
            };
        };
    m_exec_conf->msg->notice(5) << "forces added to BondSwap updater" << endl;
    };


/*!
    \param timestep Current time step of the simulation
*/
void BondSwap::update(unsigned int timestep)
    {
    if (m_prof) m_prof->push("BondSwap");
    //Most of the work is done by calling member functions. Workflow for this update:
    //(1) look for sets of particles that satisfy the distance and chain_position requirements for a swap
    //(2) evaluate the energetic cost of these proposed swaps.
    //      since a swap is not found every time, only rebuild the auxiliary bond/angle/dihedral tables if necessary
    //(3) If one is accepted, change bonds, angles, dihedrals, and force an update of the nlist
    //      and forceComputes.

    unsigned int local_rank = m_exec_conf->getRank();

    //quads holds (i, i+1,j,j+1,bond_idx1,bond_idx2) tags (4) and bond indices (2) for a swap sequence
    std::vector<std::vector<int> > quads;

    //Step (1)
    findPotentialSwaps(quads,timestep);

    //Step (2)
    if (m_prof) m_prof->push("attemptSwap");
    std::vector<std::vector<std::vector<std::vector<unsigned int> > > >angle_change_all;
    std::vector<std::vector<std::vector<std::vector<unsigned int> > > >dihedral_change_all;
    for (unsigned int qs = 0; qs  < quads.size(); ++qs)
        {
        std::vector<std::vector<std::vector<unsigned int> > > angle_change_local;
        std::vector<std::vector<std::vector<unsigned int> > > dihedral_change_local;
        bool success = attemptSwap(quads[qs],timestep,m_bond_table,m_angle_table,m_dihedral_table,
                                angle_change_local, dihedral_change_local);
        angle_change_all.push_back(angle_change_local);
        dihedral_change_all.push_back(dihedral_change_local);
        if(success) quads[qs][6] = 1;
        };
    if (m_prof) m_prof->pop();


    //Step (3) if any swaps were found to be good
    if (m_prof) m_prof->push("ChangeLoop");
    bool accept_any = false;
    int attempt_num = 0;
    int accept_num = 0;
    std::vector<std::vector<std::vector<unsigned int> > > angle_change;
    std::vector<std::vector<std::vector<unsigned int> > > dihedral_change;
    std::vector<std::vector<unsigned int> > accepted_sets;
    unsigned int iTag,jTag,swap_tag_i,swap_tag_j,b1_idx,b2_idx;
    for (unsigned int qs = 0; qs < quads.size(); ++qs)
        {
        std::vector<int> quad = quads[qs];
        if(quad[0] >= 0)
            attempt_num += 1;
        if (quad.size() > 6 && quad[6] == 1)
            {
            accept_any=true;
            //Step (3)
            iTag = quad[0];swap_tag_i = quad[1];
            jTag = quad[2];swap_tag_j = quad[3];
            b1_idx = quad[4]; b2_idx = quad[5];
            //To avoid messing up bonds and exclusions, need to avoid swapping a bond that had a node
            //that was already modified during this timestep... this chunk of code does that.
            bool already_modified = false;
            for (unsigned int cc = 0; cc < accepted_sets.size(); ++cc)
                {
                std::vector<unsigned int> cur_set = accepted_sets[cc];
                for (unsigned int ll = 0; ll < cur_set.size(); ++ll)
                    if(iTag==cur_set[ll]||jTag==cur_set[ll]||swap_tag_i==cur_set[ll]||swap_tag_j==cur_set[ll])
                        already_modified = true;
                };
            if(already_modified)
                {
                attempt_num -= 1;
                continue;
                };
            accept_num += 1;
            std::vector<unsigned int> good_set(4); good_set[0]=iTag;
            good_set[1]=jTag; good_set[2]=swap_tag_i; good_set[3]=swap_tag_j;
            accepted_sets.push_back(good_set);


            //change bonded interaction pairs
            changeBondByTag(iTag,swap_tag_i,iTag,swap_tag_j);
            changeBondByTag(jTag,swap_tag_j,jTag,swap_tag_i);
            m_exec_conf->msg->notice(5) << "changed the " << iTag <<"-" <<swap_tag_i << "  and " <<  jTag <<"-"
                    <<swap_tag_j << " bonds on rank " << local_rank <<  " at timestep " << timestep <<  endl;

            if (!m_safety_mode)
                {
                //swap the bond exclusions
                m_nlist->swapExclusion(iTag,swap_tag_i,jTag,swap_tag_j);
                //Update the auxiliary bond table
                for (unsigned int bb = 0; bb < m_bond_table[iTag].size()/3; ++bb)
                    if(m_bond_table[iTag][3*bb] == swap_tag_i && m_bond_table[iTag][3*bb+2]==b1_idx)
                        m_bond_table[iTag][3*bb] = swap_tag_j;
                for (unsigned int bb = 0; bb < m_bond_table[swap_tag_i].size()/3; ++bb)
                    if(m_bond_table[swap_tag_i][3*bb] == iTag && m_bond_table[swap_tag_i][3*bb+2]==b1_idx)
                        {m_bond_table[swap_tag_i][3*bb] = jTag;m_bond_table[swap_tag_i][3*bb+2] = b2_idx;};
                for (unsigned int bb = 0; bb < m_bond_table[jTag].size()/3; ++bb)
                    if(m_bond_table[jTag][3*bb] == swap_tag_j && m_bond_table[jTag][3*bb+2]==b2_idx)
                        m_bond_table[jTag][3*bb] = swap_tag_i;
                for (unsigned int bb = 0; bb < m_bond_table[swap_tag_j].size()/3; ++bb)
                    if(m_bond_table[swap_tag_j][3*bb] == jTag && m_bond_table[swap_tag_j][3*bb+2]==b2_idx)
                        {m_bond_table[swap_tag_j][3*bb] = iTag; m_bond_table[swap_tag_j][3*bb+2] = b1_idx;};
                };


            //If needed, swap angles
            angle_change = angle_change_all[qs];
            if(m_check_angle && angle_change.size() == 2)
                {
                for(unsigned int ai = 0; ai < angle_change[0].size(); ++ai)
                    {
                    std::vector<unsigned int> ang(3);
                    std::vector<unsigned int> ang2(3);
                    ang[0] = angle_change[0][ai][0];
                    ang[1] = angle_change[0][ai][1];
                    ang[2] = angle_change[0][ai][2];
                    ang2[0] = angle_change[1][ai][0];
                    ang2[1] = angle_change[1][ai][1];
                    ang2[2] = angle_change[1][ai][2];
                    changeAngleByTag(ang[0],ang[1],ang[2],ang2[0],ang2[1],ang2[2]);
                    m_exec_conf->msg->notice(5)  << "changed the " <<ang[0]<<"-"<<ang[1]<<"-"<<ang[2] << " angle to "
                                                <<ang2[0]<<"-"<<ang2[1]<<"-"<<ang2[2]  <<std::endl;

                    if(!m_safety_mode)
                        {
                        //change the m_angle_table entries as appropriate...
                        for (unsigned int aj = 0; aj < 3 ; ++aj)
                            {
                            unsigned int id1 = ang[aj];
                            std::vector<unsigned int> old_ang(4,0);
                            std::vector<unsigned int> new_ang(4,0);
                            new_ang[0]=ang2[0]; new_ang[1]=ang2[1]; new_ang[2]=ang2[2];
                            std::vector< std::vector<unsigned int > > new_table_entry;
                            for (unsigned int aa=0; aa < m_angle_table[id1].size(); ++aa)
                                {
                                old_ang = m_angle_table[id1][aa];
                                //if old_ang is not the angle to be replaced,
                                //add it to new_table_entry, otherwise note the index
                                bool ang_select = (old_ang[0]==ang[0]&&old_ang[1]==ang[1]&&old_ang[2]==ang[2])||
                                                  (old_ang[2]==ang[0]&&old_ang[1]==ang[1]&&old_ang[0]==ang[2]);
                                if (ang_select)
                                    {
                                    new_ang[3] = old_ang[3];
                                    }
                                else
                                    {
                                    new_table_entry.push_back(old_ang);
                                    };
                                };
                            if(id1 == ang2[0] || id1==ang2[1] || id1==ang2[2])
                                new_table_entry.push_back(new_ang);
                            m_angle_table[id1] = new_table_entry;
                            //finally, make sure the new angle appears in the spots for the final state of the angle
                            if(id1 !=  ang2[0] && id1 != ang2[1] && id1!=ang2[2])
                                m_angle_table[ang2[aj]].push_back(new_ang);
                            };

                        //update angular exclusions if necessary
                        //default HOOMD behavior is not to exclude the 1-3 or 1-4 interactions...
                        if(m_excl_angle)
                            {
                            for (unsigned int ai = 0; ai < angle_change[0].size()/2; ++ai)
                                {
                                unsigned int a_idx = 2*ai;
                                unsigned int ai1,ai2,aj1,aj2;
                                ai1 = angle_change[0][a_idx][0];
                                ai2 = angle_change[0][a_idx][2];
                                aj1 = angle_change[0][a_idx+1][0];
                                aj2 = angle_change[0][a_idx+1][2];
                                m_nlist->swapExclusion(ai1,ai2,aj1,aj2);
                                };
                            };
                        };
                    };
                };

            //If needed, swap dihedrals
            dihedral_change = dihedral_change_all[qs];
            if(m_check_dihedral && dihedral_change.size()==2&&dihedral_change[0].size()==dihedral_change[1].size())
                {
                for(unsigned int di = 0; di < dihedral_change[0].size(); ++di)
                    {
                    std::vector<unsigned int> dihed(4);
                    std::vector<unsigned int> dihed2(3);
                    dihed[0] = dihedral_change[0][di][0];
                    dihed[1] = dihedral_change[0][di][1];
                    dihed[2] = dihedral_change[0][di][2];
                    dihed[3] = dihedral_change[0][di][3];
                    dihed2[0] = dihedral_change[1][di][0];
                    dihed2[1] = dihedral_change[1][di][1];
                    dihed2[2] = dihedral_change[1][di][2];
                    dihed2[3] = dihedral_change[1][di][3];
                    changeDihedralByTag(dihed[0],dihed[1],dihed[2],dihed[3],dihed2[0],dihed2[1],dihed2[2],dihed2[3]);
                    m_exec_conf->msg->notice(5) << "swap "<<dihed[0]<<"-"<<dihed[1]<<"-"<<dihed[2] <<"-"<<dihed[3]
                         << " dihedral with "<< dihed2[0]<<"-"<<dihed2[1]<<"-"<<dihed2[2]<<"-"<<dihed2[3]<<std::endl;

                    if (!m_safety_mode)
                        {
                        //change the m_dihedral_table entries as appropriate...
                        for (unsigned int aj = 0; aj < 4 ; ++aj)
                            {
                            unsigned int id1 = dihed[aj];
                            std::vector<unsigned int> old_dih(5,0);
                            std::vector<unsigned int> new_dih(5,0);
                            new_dih[0]=dihed2[0]; new_dih[1]=dihed2[1]; new_dih[2]=dihed2[2];new_dih[3]=dihed2[3];
                            std::vector< std::vector<unsigned int > > new_table_entry;
                            for (unsigned int aa=0; aa < m_dihedral_table[id1].size(); ++aa)
                                {
                                old_dih = m_dihedral_table[id1][aa];
                                //if old_dih is not the dihedral to be replaced,
                                //add it to new_table_entry, otherwise note the index
                                bool dih_select = (old_dih[0]==dihed[0]&&old_dih[1]==dihed[1]&&
                                                   old_dih[2]==dihed[2]&&old_dih[3]==dihed[3]) ||
                                                  (old_dih[3]==dihed[0]&&old_dih[2]==dihed[1]&&
                                                   old_dih[1]==dihed[2]&&old_dih[0]==dihed[3]);
                                if (dih_select)
                                    {
                                    new_dih[4] = old_dih[4];
                                    }
                                else
                                    {
                                    new_table_entry.push_back(old_dih);
                                    };
                                };
                            if(id1 == dihed2[0] || id1==dihed2[1] || id1==dihed2[2] || id1 == dihed2[3])
                                new_table_entry.push_back(new_dih);
                            m_dihedral_table[id1] = new_table_entry;
                            //finally, make sure the new dihedral appears in the spots for the final state of the dihedral
                            //but note (see below) that the last dihedral_change element may be a purely internal dihedral
                            //rearrangement
                            bool last_dih = true;
                            if (di+1 == dihedral_change[0].size())
                                if( dihedral_change[0].size() % 2  ==1 && dihedral_change[0].size() == 5 )
                                    last_dih=false;
                            if(id1 !=  dihed2[0]&&id1!=dihed2[1]&&id1!=dihed2[2]&&id1!=dihed2[3] && last_dih)
                                m_dihedral_table[dihed2[aj]].push_back(new_dih);
                            };
                        //update dihedral exclusions if necessary
                        if(m_excl_dihedral)
                            {
                            //there can be an odd number of dihedral swaps at the center of a chain
                            //(depending on the labeling). In that case, the very last dihedral swap
                            //only rearranges internal monomers (enforced by the findNearbyDihedrals
                            //function), so no exclusions need to be changed
                            for (unsigned int di = 0; di < dihedral_change[0].size()/2; ++di)
                                {
                                unsigned int d_idx = 2*di;
                                if (dihedral_change[0].size() > d_idx+1)
                                    {
                                    unsigned int di1,di2,dj1,dj2;
                                    di1 = dihedral_change[0][d_idx][0];
                                    di2 = dihedral_change[0][d_idx][3];
                                    dj1 = dihedral_change[0][d_idx+1][0];
                                    dj2 = dihedral_change[0][d_idx+1][3];
                                    m_nlist->swapExclusion(di1,di2,dj1,dj2);
                                    };
                                };
                            };
                        };
                    };
                };//end dihedral if
                if(m_check_angle || m_check_dihedral)
                    break;
            }; //this is attached to the loop over possible swap sets
        };
    ArrayHandle<int> h_mc_stats(m_mc_stats,access_location::host, access_mode::readwrite);
    h_mc_stats.data[0]=h_mc_stats.data[0] + attempt_num;
    h_mc_stats.data[1]=h_mc_stats.data[1] + accept_num;

    if (m_prof) m_prof->pop(); //profiler for the change loop

    if (m_prof) m_prof->push("RecomputeNlistForces");
    if(accept_any)
        {
        m_sysdef->getBondData()->setDirty();
        if(m_safety_mode)
            {
            m_nlist->clearExclusions();
            m_nlist->addExclusionsFromBonds();
            buildBondTable(m_bond_table);
            };
        if(m_check_angle)
            {
            if(m_safety_mode)
                {
                if (m_excl_angle) m_nlist->addExclusionsFromAngles();
                buildAngleTable(m_angle_table);
                };
            m_sysdef->getAngleData()->setDirty();
            };
        if(m_check_dihedral)
            {
            if(m_safety_mode)
                {
                if (m_excl_dihedral) m_nlist->addExclusionsFromDihedrals();
                buildDihedralTable(m_dihedral_table);
                };
            m_sysdef->getDihedralData()->setDirty();
           };

        //flag the neighbor list to be updated
        m_nlist->forceUpdate();

        }

    if (m_prof) m_prof->pop(); //profiler for the rebuild stage

    if (m_prof) m_prof->pop(); //profiler for the whole class
    }

/*!
    \param quads vector of vector of particle tags and bond types of swap candidates
    \param timestep Timestep of the simulation (used as second input to saru RNG)
*/
void BondSwap::findPotentialSwaps(std::vector< std::vector<int> > &quads, int timestep)
    {

    if (m_prof) m_prof->push("findSwaps");

    // access the particle data, chain position data, box, and neighbor list
    if (m_prof) m_prof->push("DataAccess");
    ArrayHandle<Scalar4> h_pos(m_pdata->getPositions(), access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_tag(m_pdata->getTags(), access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_chain_position(m_chain_position,access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_n_neigh(m_nlist->getNNeighArray(),access_location::host,access_mode::read);
    ArrayHandle<unsigned int> h_nlist(m_nlist->getNListArray(),access_location::host,access_mode::read);
    ArrayHandle<unsigned int> h_head_list(m_nlist->getHeadList(),access_location::host,access_mode::read);
    if (m_prof) m_prof->pop();

    const BoxDim& box = m_pdata->getGlobalBox();

    std::vector<int> quad(7,-1);
    int swaps_found = 0;
    bool mpi= (m_pdata->getDomainDecomposition());

    //This routine first looks for pairs with matching chain positions.
    //If found, check other distances of bond swap partners: (i,i+1), (j,j+1), (i,j+1), (j, i+1) all less than
    //m_distSquared. Also ensure the four atoms are distinct, and that i+1,j+1 have matching chain positions.
    //If yes, we have a swap candidate.

    //IMPLEMENTATION NOTE: this assumes that the (i,i+1) and (j,j+1) pairs themselves are sufficiently close
    //(as a result of existing bonds).

    //first, consider only a fraction of particles for swapping:
    unsigned int loop_size = floor(((Scalar)m_fraction*(m_pdata->getN())));
    if (m_prof) m_prof->push("SwapSearch");
    for (unsigned int i = 0; i < loop_size; ++i)
        {
        //get tag and position of particle i
        unsigned int iTag = h_tag.data[i];
        Scalar3 pi = make_scalar3(h_pos.data[i].x, h_pos.data[i].y,  h_pos.data[i].z);
        unsigned int chainPosI = h_chain_position.data[iTag];

        //loop over all neighbors of this particle
        const unsigned int myHead = h_head_list.data[i];
        const unsigned int nsize = (unsigned int)h_n_neigh.data[i];
        for (unsigned int k = 0; k < nsize; ++k)
            {
            //get index, tag, and chain position of this neighbor...see if i and j are close enough
            unsigned int j = h_nlist.data[myHead+k];
            assert(j < m_pdata->getN() + m_pdata->getNGhosts());
            unsigned int jTag = h_tag.data[j];
            if (mpi)
                {
                if (!m_pdata->isParticleLocal(jTag)) continue;
                };
            assert(m_pdata->isParticleLocal(jTag));
            assert(m_pdata->isParticleLocal(iTag));
            unsigned int chainPosJ = h_chain_position.data[jTag];
            if(chainPosI==chainPosJ)
                {
                //i and j match chain position and are close. see if they have bonded (i+-1, j+-1) that also match
                for (unsigned int b1 = 0; b1 < m_bond_table[iTag].size()/3; ++b1)
                    for (unsigned int b2 = 0; b2 < m_bond_table[jTag].size()/3; ++b2)
                        {// loop over matching chain positions
                        if (m_bond_table[iTag][b1*3+1] == m_bond_table[jTag][b2*3+1])
                            {//check distances...need the index of particle with the given tag
                            if (mpi)
                                {
                                if (!(m_pdata->isParticleLocal(m_bond_table[iTag][b1*3+1])))
                                    break;
                                if (!(m_pdata->isParticleLocal(m_bond_table[jTag][b2*3+1])))
                                    break;
                                };
                            unsigned int idx_in = m_pdata->getRTag(m_bond_table[iTag][b1*3]);
                            unsigned int idx_jn = m_pdata->getRTag(m_bond_table[jTag][b2*3]);
                            // don't double the set of candidates
                            if(j>idx_jn || i > idx_in || j < i) continue;
                            // necessary check for swaps between ends of the same chain
                            if(idx_in == idx_jn || idx_in == j) continue;

                            Scalar3 p_jn=make_scalar3(h_pos.data[idx_jn].x,h_pos.data[idx_jn].y,h_pos.data[idx_jn].z);
                            Scalar3 dx1 = pi-p_jn;
                            dx1 = box.minImage(dx1);
                            Scalar rsq1 = dot(dx1,dx1);
                            if(rsq1 <=m_distSquared)
                                {
                                Scalar3 pj = make_scalar3(h_pos.data[j].x, h_pos.data[j].y,  h_pos.data[j].z);
                                Scalar3 p_in=make_scalar3(h_pos.data[idx_in].x,
                                                          h_pos.data[idx_in].y,h_pos.data[idx_in].z);
                                Scalar3 dx2 = pj-p_in;
                                dx2=box.minImage(dx2);
                                Scalar rsq2 = dot(dx2,dx2);
                                if(rsq2 <= m_distSquared)
                                    {//found a potential swap set
                                    quad[0]=iTag;quad[1]=m_bond_table[iTag][b1*3];
                                    quad[2]=jTag;quad[3]=m_bond_table[jTag][b2*3];
                                    quad[4] = m_bond_table[iTag][b1*3+2];quad[5] = m_bond_table[jTag][b2*3+2];
                                    //cout << iTag << "-" << quad[1] << "    " << jTag <<"-" <<quad[3] << endl;
                                    quads.push_back(quad);
                                    swaps_found +=1;
                                    //Only try a single swap?
                                    //return;
                                    }
                                };

                            };

                        };

                };//ends check that chainPosI==chainPosJ
            }; //ends loop over neighbors of i
        };
    if (m_prof) m_prof->pop();

    //in the main update loop quads should have at least one element... if empty feed it a junk set
    if (swaps_found == 0 ) quads.push_back(quad);

    if (m_prof) m_prof->pop();
    }



/*!
    \param quad vector of particle tags and bond types
    \param timestep Timestep of the simulation (used as second input to saru RNG)
    \param angle_table auxiliary structure for identifying angles
    \param dihedral_table auxiliary structure for identifying dihedrals
    \param angle_change vector of vector of vector with information about angles to be added and removed
    \param dihedral_change vector of vector of vector with information about dihedrals to be added and removed
    The attemptSwap function is the only place that energetics are computed.
*/
bool BondSwap::attemptSwap(std::vector<int> &quad, int timestep,
                        std::vector< std::vector<unsigned int> > &bond_table,
                        std::vector<std::vector<std::vector<unsigned int> > > &angle_table,
                        std::vector<std::vector<std::vector<unsigned int> > > &dihedral_table,
                        std::vector<std::vector<std::vector<unsigned int> > > &angle_change,
                        std::vector<std::vector<std::vector<unsigned int> > > &dihedral_change)
    {
    if (quad[0]<0) return false;
    Saru saru(m_seed,timestep);

    unsigned int iTag = quad[0];
    unsigned int swap_tag_i=quad[1];
    unsigned int jTag = quad[2];
    unsigned int swap_tag_j=quad[3];
    unsigned int bond_i1=quad[4];
    unsigned int bond_i2=quad[5];

    //compute the energy cost of a bond change
    Scalar energy_i = getEnergyPairBond(iTag,swap_tag_i,jTag,swap_tag_j,bond_i1,bond_i2);
    Scalar energy_f = getEnergyPairBond(iTag,swap_tag_j,jTag,swap_tag_i,bond_i1,bond_i2);

    std::vector< std::vector<unsigned int> > initial_angles;
    std::vector< std::vector<unsigned int> > final_angles;
    //if there are angles or dihedrals, include the cost of switching them...
    if(m_check_angle)
        {
        //call a function to find the angles that would be changed by the proposed bond swap
        findNearbyAngles(iTag,jTag,swap_tag_i,swap_tag_j,initial_angles,final_angles,bond_table,angle_table);

        //we now have the set of angles in the initial and final states...add them to the energy costs.
        Scalar ang_en_i = 0.0;
        Scalar ang_en_f = 0.0;
        unsigned int angle_idx = 0;
        for (unsigned int ai = 0; ai < initial_angles.size(); ++ai)
            {
            ang_en_i += getEnergyAngle(initial_angles[ai][0],initial_angles[ai][1],initial_angles[ai][2],angle_idx);
            };
        for (unsigned int ai = 0; ai < final_angles.size(); ++ai)
            {
            ang_en_f += getEnergyAngle(final_angles[ai][0],final_angles[ai][1],final_angles[ai][2],angle_idx);
            };

        energy_i += ang_en_i;
        energy_f += ang_en_f;
        }

    std::vector< std::vector<unsigned int> > initial_dihedrals;
    std::vector< std::vector<unsigned int> > final_dihedrals;
    if(m_check_dihedral)
        {
        //get the set of dihedrals to possibly change
        findNearbyDihedrals(iTag,jTag,swap_tag_i,swap_tag_j,initial_dihedrals,final_dihedrals,
                            bond_table,dihedral_table);
        //add the relevant angles to both energy_i and energy_f
        Scalar dih_en_i = 0.0;
        Scalar dih_en_f = 0.0;
        unsigned int dih_idx = 0;
        for (unsigned int d = 0; d < initial_dihedrals.size(); ++d)
            {
            std::vector<unsigned int> dih = initial_dihedrals[d];
            dih_en_i += getEnergyDihedral(dih[0],dih[1],dih[2],dih[3],dih_idx);
            };
        for (unsigned int d = 0; d < final_dihedrals.size(); ++d)
            {
            std::vector<unsigned int> dih = final_dihedrals[d];
            dih_en_f += getEnergyDihedral(dih[0],dih[1],dih[2],dih[3],dih_idx);
            };
        energy_i += dih_en_i;
        energy_f += dih_en_f;
        };

    Scalar e_diff = energy_f - energy_i;
    Scalar cost = exp(-e_diff/m_Tnew);
    if(saru.f(0,1) < cost)
        {
        m_exec_conf->msg->notice(6) << " Energy changed by " << e_diff << endl;
        if(m_check_angle)
            {
            angle_change.push_back(initial_angles);
            angle_change.push_back(final_angles);
            };
        if(m_check_dihedral)
            {
            dihedral_change.push_back(initial_dihedrals);
            dihedral_change.push_back(final_dihedrals);
            };
        return true;
        };

    return false;
    }


/*! \param pi1 Tag of a particle
    \param pi2 Tag of a particle
    \param pj1 Tag of a particle
    \param pj2 Tag of a particle
    \param bond_ind1 Index of a bond
    \param bond_ind2 Index of a bond
*/
Scalar BondSwap::getEnergyPairBond(unsigned int pi1, unsigned int pi2, unsigned int pj1, unsigned int pj2,
                                   unsigned int bond_ind1, unsigned int bond_ind2)
    {
    std::vector<unsigned int> pair1(2);
    std::vector<unsigned int> pair2(2);
    std::vector<unsigned int> bond1(3);
    std::vector<unsigned int> bond2(3);
    pair1[0]=pi1; pair1[1]=pj2;
    pair2[0]=pj1; pair2[1]=pi2;
    bond1[0]=pi1; bond1[1] = pi2;bond1[2]=bond_ind1;
    bond2[0]=pj1; bond2[1] = pj2;bond2[2]=bond_ind2;
    Scalar ep1, ep2, eb1, eb2;

    //iterate over force computes
    std::vector< boost::shared_ptr<ForceCompute> >::iterator force_compute;
    for (force_compute = m_forces.begin(); force_compute != m_forces.end(); ++force_compute)
        {
        std::vector< std::string > f_log_name;
        f_log_name = (*force_compute)->getProvidedLogQuantities();
        for (unsigned int name = 0; name < f_log_name.size(); ++name)
            {
            unsigned int ftype = forceComputeType(f_log_name[name]);
            switch(ftype)
                {
                case 1:
                    //pairs
                    (*force_compute)->computeEnergySet(pair1,ep1);
                    (*force_compute)->computeEnergySet(pair2,ep2);
                    break;
                case 2:
                    //bonds
                    (*force_compute)->computeEnergySet(bond1,eb1);
                    (*force_compute)->computeEnergySet(bond2,eb2);
                    break;
                default:
                    break;
                };
            };
        };

    return ep1+ep2+eb1+eb2;
    };

/*! \param forceUpdate Boolean for forcing an update of all force computes known by the bond swapper
    \param timestep Current timestep.
    \post if forceUpdate is true, forceComputes will be recomputed
*/
Scalar BondSwap::getEnergyStep(bool forceUpdate, unsigned int timestep)
    {
    Scalar energy = 0.0;
    std::vector< boost::shared_ptr<ForceCompute> >::iterator force_compute;
    for (force_compute = m_forces.begin(); force_compute != m_forces.end(); ++force_compute)
        {
        if (forceUpdate)
            (*force_compute)->forceCompute(timestep);
        energy += (*force_compute) ->calcEnergySum();
        };

    return energy;
    };

/*! \param bi1 Initial tag of one particle in a bond
    \param bi2 Inital tag of other particle in bond
    \param bf1 Final desired tag associated with the bond
    \param bf2 Final desired tag associated with the bond
    \post (bi1,bi2) is not a bonded pair. (bf1,bf2) is a bonded pair
*/
void BondSwap::changeBondByTag(unsigned int bi1, unsigned int bi2, unsigned int bf1, unsigned int bf2)
    {
    boost::shared_ptr<BondData> initialBonds = m_sysdef->getBondData();
    ArrayHandle<BondData::members_t> h_bonds(initialBonds->getMembersArray(),
                                             access_location::host,access_mode::readwrite);
    unsigned int bsize = (unsigned int) initialBonds->getN();
    for (unsigned int bi = 0; bi < bsize; ++bi)
        {
        const BondData::members_t& bond = h_bonds.data[bi];
        if (bond.tag[0] == bi1 &&bond.tag[1] == bi2)
            {
            h_bonds.data[bi].tag[0]=bf1;
            h_bonds.data[bi].tag[1]=bf2;
            break;
            }
        if (bond.tag[0] == bi2 &&bond.tag[1] == bi1)
            {
            h_bonds.data[bi].tag[0]=bf1;
            h_bonds.data[bi].tag[1]=bf2;
            break;
            }
        };
    };

/*! \param ai1 initial tag for one end of an angle
    \param ai2 middle tag for an angle
    \param ai3 final tag for an angle
    \param af1 final desired tag
    \param af2 final desired tag
    \param af3 final desired tag
    \post (ai1,ai2,ai3) or (ai3,ai2,ai1) is not a angle triplet. (af1,af2,af3) is an angle triplet.
*/
void BondSwap::changeAngleByTag(unsigned int ai1, unsigned int ai2, unsigned int ai3,
                                unsigned int af1, unsigned int af2, unsigned int af3)
    {
    boost::shared_ptr<AngleData> angles = m_sysdef->getAngleData();
    ArrayHandle<AngleData::members_t> h_angles(angles->getMembersArray(),
                                               access_location::host,access_mode::readwrite);
    unsigned int asize = angles->getN();
    bool replaced = false;
    for (unsigned int a = 0;a < asize; ++a)
        {
        const AngleData::members_t& ang = h_angles.data[a];
        if(ang.tag[0] == ai1 && ang.tag[1] == ai2 && ang.tag[2] ==ai3)
            {
            h_angles.data[a].tag[0] = af1;
            h_angles.data[a].tag[1] = af2;
            h_angles.data[a].tag[2] = af3;
            replaced = true;
            break;
            };
        if(ang.tag[2] == ai1 && ang.tag[1] == ai2 && ang.tag[0] ==ai3)
            {
            h_angles.data[a].tag[0] = af1;
            h_angles.data[a].tag[1] = af2;
            h_angles.data[a].tag[2] = af3;
            replaced = true;
            break;
            };
        };
    if(!replaced)
        {
        m_exec_conf->msg->error() << "Attempted to replace an angle that was not found!" << std::endl
                                            << "  " <<ai1 <<"   " << ai2 << "   " << ai3 << std::endl;
        }
    };

/*! \param pi1 particle tag for start of angle
    \param pi2 particle tag for middle of angle
    \param pi3 particle tag for end of angle
    \param angle_index index of the angle according to which to compute the energy
*/
Scalar BondSwap::getEnergyAngle(unsigned int pi1, unsigned int pi2, unsigned int pi3, unsigned int angle_index)
    {
    Scalar energy = 0.0;
    std::vector<unsigned int> angle(4);
    angle[0]=pi1;angle[1]=pi2;angle[2]=pi3;angle[3]=angle_index;

    //iterate over force computes...find an angle
    std::vector< boost::shared_ptr<ForceCompute> >::iterator force_compute;
    for (force_compute = m_forces.begin(); force_compute != m_forces.end(); ++force_compute)
        {
        std::vector< std::string > f_log_name;
        f_log_name = (*force_compute)->getProvidedLogQuantities();
        for (unsigned int name = 0; name < f_log_name.size(); ++name)
            {
            unsigned int ftype = forceComputeType(f_log_name[name]);
            switch(ftype)
                {
                case 3:
                    //angles
                    (*force_compute)->computeEnergySet(angle,energy);
                    break;
                default:
                    break;
                };
            };
        };
    return energy;
    };

/*!
    \param iTag tag of i
    \param jTag tag of j
    \param swap_tag_i tag of particle bonded to i
    \param swap_tag_j tag of particle bonded to j
    \param initial_angles vector filled with any angle that includes
                    (i,swap_tag_i) or (j,swap_tag_j) as two of its three tags
    \param final_angles vector will be filled with any angle that
                    includes (i,swap_tag_j) or (j,swap_tag_i) as two of its three tags
    \param bond_table auxiliary structure for identifying bonds
    \param angle_table auxiliary structure for identifying angles
*/
void BondSwap::findNearbyAngles(unsigned int iTag, unsigned int jTag,
                                unsigned int swap_tag_i, unsigned int swap_tag_j,
                                std::vector< std::vector<unsigned int> > &initial_angles,
                                std::vector< std::vector<unsigned int> > &final_angles,
                                std::vector< std::vector<unsigned int> > &bond_table,
                                std::vector<std::vector<std::vector<unsigned int> > > &angle_table)
    {
    //First, let's deduce the order of particles in the relevant angles...
    //iprev is in an angle with iTag and swap_tag_i, and is bonded to iTag
    //inext is in an angle with iTag and swap_tag_i, and is bonded to swap_tag_i
    //ditto for jprev and jnext. Near the end of a chain any of these can be non-existant
    int iprev,inext,jprev,jnext;
    iprev=inext=jprev=jnext = -1;
    unsigned int i_length = 2;
    unsigned int j_length = 2;
    for (unsigned int a = 0; a < angle_table[iTag].size(); ++a)
        {
        std::vector<unsigned int> ang = angle_table[iTag][a];
        int i_candidate=-1;
        //check every possible position in the angle for iTag and swap_tag_i...
        if(ang[0] == iTag && ang[1] == swap_tag_i)
            i_candidate = ang[2];
        if(ang[1] == iTag && ang[0] == swap_tag_i)
            i_candidate = ang[2];
        if(ang[2] == iTag && ang[1] == swap_tag_i)
            i_candidate = ang[0];
        if(ang[1] == iTag && ang[2] == swap_tag_i)
            i_candidate = ang[0];
        //if a candidate has been found, see what it is bonded to and assign it correctly
        if (i_candidate != -1)
            {
            for (unsigned int b = 0; b < bond_table[i_candidate].size()/3; ++b)
                {
                unsigned int bond_partner = bond_table[i_candidate][3*b];
                if (bond_partner == iTag) iprev = i_candidate;
                if (bond_partner == swap_tag_i) inext = i_candidate;
                };
            }
        }
    //repeat to find jprev and jnext
    for (unsigned int a = 0; a < angle_table[jTag].size(); ++a)
        {
        std::vector<unsigned int> ang = angle_table[jTag][a];
        int j_candidate=-1;
        if(ang[0] == jTag && ang[1] == swap_tag_j)
            j_candidate = ang[2];
        if(ang[1] == jTag && ang[0] == swap_tag_j)
            j_candidate = ang[2];
        if(ang[2] == jTag && ang[1] == swap_tag_j)
            j_candidate = ang[0];
        if(ang[1] == jTag && ang[2] == swap_tag_j)
            j_candidate = ang[0];
        if (j_candidate != -1)
            {
            for (unsigned int b = 0; b < bond_table[j_candidate].size()/3; ++b)
                {
                unsigned int bond_partner = bond_table[j_candidate][3*b];
                if (bond_partner == jTag) jprev = j_candidate;
                if (bond_partner == swap_tag_j) jnext = j_candidate;
                };
            }
        }
    if (inext != -1) i_length += 1;
    if (iprev != -1) i_length += 1;
    if (jnext != -1) j_length += 1;
    if (jprev != -1) j_length += 1;

    //we now have iprev-iTag-swap_tag_i-inext set and corresponding j set
    //figure out what angles will be removed (angles_initial) and added (angles_final)
    std::vector<unsigned int> new_angle(3);
    //this first if block covers the default operating case, in which iprev = -1 --> jprev = -1,
    //and chain lengths are conserved
    if ((i_length == j_length)&& ( (i_length==4) || ( iprev==jprev || inext == jnext ) ) )
        {
        if (iprev != -1)
            {
            new_angle[0]=iprev;new_angle[1]=iTag;new_angle[2] = swap_tag_i;
            initial_angles.push_back(new_angle);
            new_angle[2]=swap_tag_j;
            final_angles.push_back(new_angle);

            new_angle[0]=jprev;new_angle[1]=jTag;new_angle[2] = swap_tag_j;
            initial_angles.push_back(new_angle);
            new_angle[2]=swap_tag_i;
            final_angles.push_back(new_angle);
            };
        if (inext != -1)
            {
            new_angle[0]=iTag;new_angle[1]=swap_tag_i;new_angle[2] = inext;
            initial_angles.push_back(new_angle);
            new_angle[1]=swap_tag_j; new_angle[2]=jnext;
            final_angles.push_back(new_angle);

            new_angle[0]=jTag;new_angle[1]=swap_tag_j;new_angle[2] = jnext;
            initial_angles.push_back(new_angle);
            new_angle[1]=swap_tag_i; new_angle[2]=inext;
            final_angles.push_back(new_angle);
            };
        };
    //if iprev = jnext = -1 and the other ends occupied (or vice versa) a dimer is created
    if ( (i_length == 3 && j_length == 3 ) && ( ( iprev == -1 && jnext == -1 ) || (inext == -1 && jprev == -1 ) ) )
        {
        //ip-iT-is -> ip-iT-js; jT-js-jn -> iT-js-jn
        if ((inext == -1) && (jprev == -1))
            {
            new_angle[0]=iprev;new_angle[1]=iTag;new_angle[2] = swap_tag_i;
            initial_angles.push_back(new_angle);
            new_angle[2]=swap_tag_j;
            final_angles.push_back(new_angle);

            new_angle[0]=jTag;new_angle[1]=swap_tag_j;new_angle[2] = jnext;
            initial_angles.push_back(new_angle);
            new_angle[0]=iTag;
            final_angles.push_back(new_angle);
            };
        //vice-versa
        if ((iprev == -1) && (jnext == -1))
            {
            new_angle[0]=jprev;new_angle[1]=jTag;new_angle[2] = swap_tag_j;
            initial_angles.push_back(new_angle);
            new_angle[2]=swap_tag_i;
            final_angles.push_back(new_angle);

            new_angle[0]=iTag;new_angle[1]=swap_tag_i;new_angle[2] = inext;
            initial_angles.push_back(new_angle);
            new_angle[0]=jTag;
            final_angles.push_back(new_angle);
            };
        };
    //angle logic for combining a dimer with something longer
    if (i_length ==2 || j_length == 2)
        {
        //first, suppose chain i is the dimer
        if (jprev != -1)
            {
            new_angle[0]=jprev;new_angle[1]=jTag;new_angle[2] = swap_tag_j;
            initial_angles.push_back(new_angle);
            new_angle[2]=swap_tag_i;
            final_angles.push_back(new_angle);
            };
        if (jnext != -1)
            {
            new_angle[0]=jTag;new_angle[1]=swap_tag_j;new_angle[2] = jnext;
            initial_angles.push_back(new_angle);
            new_angle[0]=iTag;
            final_angles.push_back(new_angle);
            };
        //vice versa
        if (iprev != -1)
            {
            new_angle[0]=iprev;new_angle[1]=iTag;new_angle[2] = swap_tag_i;
            initial_angles.push_back(new_angle);
            new_angle[2]=swap_tag_j;
            final_angles.push_back(new_angle);
            };
        if (inext != -1)
            {
            new_angle[0]=iTag;new_angle[1]=swap_tag_i;new_angle[2] = inext;
            initial_angles.push_back(new_angle);
            new_angle[0]=jTag;
            final_angles.push_back(new_angle);
            };
        };
    //finally, here is the angle logic for combining length 3 with length 4
    //first, i is the short chain
    if( i_length == 3 && j_length ==4)
        {
        if (inext != -1)
            {
            new_angle[0]=iTag;new_angle[1]=swap_tag_i;new_angle[2] = inext;
            initial_angles.push_back(new_angle);
            new_angle[1]=swap_tag_j; new_angle[2] = jnext;
            final_angles.push_back(new_angle);

            new_angle[0]=jprev;new_angle[1]=jTag;new_angle[2] = swap_tag_j;
            initial_angles.push_back(new_angle);
            new_angle[2] = swap_tag_i;
            final_angles.push_back(new_angle);

            new_angle[0]=jTag;new_angle[1]=swap_tag_j;new_angle[2] = jnext;
            initial_angles.push_back(new_angle);
            new_angle[1]=swap_tag_i; new_angle[2] = inext;
            final_angles.push_back(new_angle);
            };
        if (iprev != -1)
            {
            new_angle[0]=jTag;new_angle[1]=swap_tag_j;new_angle[2] = jnext;
            initial_angles.push_back(new_angle);
            new_angle[0]=iTag;
            final_angles.push_back(new_angle);

            new_angle[0]=jprev;new_angle[1]=jTag;new_angle[2] = swap_tag_j;
            initial_angles.push_back(new_angle);
            new_angle[2]=swap_tag_i;
            final_angles.push_back(new_angle);

            new_angle[0]=iprev;new_angle[1]=iTag;new_angle[2] = swap_tag_i;
            initial_angles.push_back(new_angle);
            new_angle[2]=swap_tag_j;
            final_angles.push_back(new_angle);
            };
        };
    //next, j is the short chain
    if( j_length == 3 && i_length ==4)
        {
        if (jnext != -1)
            {
            new_angle[0]=jTag;new_angle[1]=swap_tag_j;new_angle[2] = jnext;
            initial_angles.push_back(new_angle);
            new_angle[1]=swap_tag_i; new_angle[2] = inext;
            final_angles.push_back(new_angle);

            new_angle[0]=iprev;new_angle[1]=iTag;new_angle[2] = swap_tag_i;
            initial_angles.push_back(new_angle);
            new_angle[2] = swap_tag_j;
            final_angles.push_back(new_angle);

            new_angle[0]=iTag;new_angle[1]=swap_tag_i;new_angle[2] = inext;
            initial_angles.push_back(new_angle);
            new_angle[1]=swap_tag_j; new_angle[2] = jnext;
            final_angles.push_back(new_angle);
            };
        if (jprev != -1)
            {
            new_angle[0]=iTag;new_angle[1]=swap_tag_i;new_angle[2] = inext;
            initial_angles.push_back(new_angle);
            new_angle[0]=jTag;
            final_angles.push_back(new_angle);

            new_angle[0]=iprev;new_angle[1]=iTag;new_angle[2] = swap_tag_i;
            initial_angles.push_back(new_angle);
            new_angle[2]=swap_tag_j;
            final_angles.push_back(new_angle);

            new_angle[0]=jprev;new_angle[1]=jTag;new_angle[2] = swap_tag_j;
            initial_angles.push_back(new_angle);
            new_angle[2]=swap_tag_i;
            final_angles.push_back(new_angle);
            };
        };


    };

/*! \param di1 initial tag for one end of a dihedral
    \param di2 middle tag for a dihedral
    \param di3 middle tag for a dihedral
    \param di4  end tag for a dihedral
    \param df1 final desired tag
    \param df2 final desired tag
    \param df3 final desired tag
    \param df4 final desired tag
    \post (di1,di2,di3,di4) or (di4,di3,di2,di1) is not a dihedral set. (df1,df2,df3,df4) is a dihedral set
*/
void BondSwap::changeDihedralByTag(unsigned int di1, unsigned int di2, unsigned int di3, unsigned int di4,
                                   unsigned int df1, unsigned int df2, unsigned int df3, unsigned int df4)
    {
    boost::shared_ptr<DihedralData> dihedrals = m_sysdef->getDihedralData();
    ArrayHandle<DihedralData::members_t> h_dihedrals(dihedrals->getMembersArray(),
                                                     access_location::host,access_mode::readwrite);
    unsigned int dsize = dihedrals->getN();
    bool replaced = false;
    for (unsigned int a = 0;a < dsize; ++a)
        {
        const DihedralData::members_t& dihed = h_dihedrals.data[a];
        if(dihed.tag[0] == di1 && dihed.tag[1] == di2 && dihed.tag[2] ==di3 && dihed.tag[3] == di4)
            {
            h_dihedrals.data[a].tag[0] = df1;
            h_dihedrals.data[a].tag[1] = df2;
            h_dihedrals.data[a].tag[2] = df3;
            h_dihedrals.data[a].tag[3] = df4;
            replaced = true;
            break;
            };
        if(dihed.tag[3] == di1 && dihed.tag[2] == di2 && dihed.tag[1] ==di3 && dihed.tag[0] == di4)
            {
            h_dihedrals.data[a].tag[0] = df1;
            h_dihedrals.data[a].tag[1] = df2;
            h_dihedrals.data[a].tag[2] = df3;
            h_dihedrals.data[a].tag[3] = df4;
            replaced = true;
            break;
            };
        };
    if(!replaced) m_exec_conf->msg->error() << "Attempted to replace a dihedral that was not found!" << std::endl
                                            << "  " <<di1 <<"   " << di2 << "   " << di3<<"  "<<di4 << std::endl;
    };

/*! \param pi1 particle tag for start of dihedral
    \param pi2 particle tag for middle of dihedral
    \param pi3 particle tag for middle of dihedral
    \param pi4 particle tag for end of dihedral
    \param dihedral_index index of the dihedral according to which to compute the energy
*/
Scalar BondSwap::getEnergyDihedral(unsigned int pi1, unsigned int pi2, unsigned int pi3, unsigned int pi4,
                                   unsigned int dihedral_index)
    {
    Scalar energy = 0.0;
    std::vector<unsigned int> dihedral(5);
    dihedral[0]=pi1;dihedral[1]=pi2;dihedral[2]=pi3;dihedral[3]=pi4; dihedral[4] = dihedral_index;
    //iterate over force computes...find a dihedral
    std::vector< boost::shared_ptr<ForceCompute> >::iterator force_compute;
    for (force_compute = m_forces.begin(); force_compute != m_forces.end(); ++force_compute)
        {
        std::vector< std::string > f_log_name;
        f_log_name = (*force_compute)->getProvidedLogQuantities();
        for (unsigned int name = 0; name < f_log_name.size(); ++name)
            {
            unsigned int ftype = forceComputeType(f_log_name[name]);
            switch(ftype)
                {
                case 4:
                    //dihedrals
                    (*force_compute)->computeEnergySet(dihedral,energy);
                    break;
                default:
                    break;
                };
            };
        };
    return energy;
    };


/*!
    \param iTag tag of i
    \param jTag tag of j
    \param swap_tag_i tag of particle bonded to i
    \param swap_tag_j tag of particle bonded to j
    \param initial_dihedrals vector will be filled with any dihedral that
                             includes (i,swap_tag_i) or (j,swap_tag_j) as two of its four tags
    \param final_dihedrals  vector will be filled with any dihedral that
                            includes (i,swap_tag_j) or (j,swap_tag_i) as two of its four tags
    \param bond_table auxiliary structure for identifying bonds
    \param dihedral_table auxiliary structure for identifying dihedrals
*/
void BondSwap::findNearbyDihedrals(unsigned int iTag, unsigned int jTag,
                                unsigned int swap_tag_i, unsigned int swap_tag_j,
                                std::vector< std::vector<unsigned int> > &initial_dihedrals,
                                std::vector< std::vector<unsigned int> > &final_dihedrals,
                                std::vector< std::vector<unsigned int> > &bond_table,
                                std::vector<std::vector<std::vector<unsigned int> > > &dihedral_table)
    {
    //first identify the ip2-iprev-iTag-swap_tag_i-inext-in2 sequence (and corresponding one for j)
    int ip2,iprev,inext,in2,jp2,jprev,jnext,jn2;
    ip2=iprev=inext=in2=jp2=jprev=jnext=jn2 = -1;
    int i_length = 2;
    int j_length = 2;
    //first, the i Chain
    for (unsigned int d = 0; d < dihedral_table[iTag].size();++d)
        {
        std::vector<unsigned int> dih = dihedral_table[iTag][d];
        if (dih[0] ==iTag &&dih[1] == swap_tag_i)
            {
            inext = dih[2];
            in2 = dih[3];
            };
        if (dih[3] ==iTag &&dih[2] == swap_tag_i)
            {
            inext = dih[1];
            in2 = dih[0];
            };
        if (dih[0] ==swap_tag_i &&dih[1] == iTag)
            {
            iprev = dih[2];
            ip2 = dih[3];
            };
        if (dih[3] ==swap_tag_i &&dih[2] == iTag)
            {
            iprev = dih[1];
            ip2 = dih[0];
            };
        //near a chain end this might be needed if, e.g. ip2 == -1 but iprev != -1
        if((dih[1]==iTag && dih[2]==swap_tag_i) || (dih[2]==iTag && dih[1] == swap_tag_i))
            {
            unsigned int ic1=dih[0];
            unsigned int ic2=dih[3];
            //which is bonded to iTag?
            for (unsigned int b = 0; b < bond_table[iTag].size(); ++b)
                {
                unsigned int bond_partner = bond_table[iTag][3*b];
                if (bond_partner == ic1)
                    {
                    if (iprev == -1) iprev=ic1;
                    if (inext == -1) inext=ic2;
                    };
                if (bond_partner == ic2)
                    {
                    if (iprev == -1) iprev=ic2;
                    if (inext == -1) inext=ic1;
                    };
                };
            };
        };

    //repeat for the j chain
    for (unsigned int d = 0; d < dihedral_table[jTag].size();++d)
        {
        std::vector<unsigned int> dih = dihedral_table[jTag][d];
        if (dih[0] ==jTag &&dih[1] == swap_tag_j)
            {
            jnext = dih[2];
            jn2 = dih[3];
            };
        if (dih[3] ==jTag &&dih[2] == swap_tag_j)
            {
            jnext = dih[1];
            jn2 = dih[0];
            };
        if (dih[0] ==swap_tag_j &&dih[1] == jTag)
            {
            jprev = dih[2];
            jp2 = dih[3];
            };
        if (dih[3] ==swap_tag_j &&dih[2] == jTag)
            {
            jprev = dih[1];
            jp2 = dih[0];
            };
        if((dih[1]==jTag && dih[2]==swap_tag_j) || (dih[2]==jTag && dih[1] == swap_tag_j))
            {
            unsigned int jc1=dih[0];
            unsigned int jc2=dih[3];
            //which is bonded to jTag?
            for (unsigned int b = 0; b < bond_table[jTag].size(); ++b)
                {
                unsigned int bond_partner = bond_table[jTag][3*b];
                if (bond_partner == jc1)
                    {
                    if (jprev == -1) jprev=jc1;
                    if (jnext == -1) jnext=jc2;
                    };
                if (bond_partner == jc2)
                    {
                    if(jprev == -1) jprev=jc2;
                    if(jnext == -1) jnext=jc1;
                    };
                };
            };
        };
    if (ip2 != -1)   i_length += 1;
    if (iprev != -1) i_length += 1;
    if (inext != -1) i_length += 1;
    if (in2 != -1)   i_length += 1;
    if (jp2 != -1)   j_length += 1;
    if (jprev != -1) j_length += 1;
    if (jnext != -1) j_length += 1;
    if (jn2 != -1)   j_length += 1;
    //we now have ip2-ip-i-is-in-in2 sets and the corresponding j set
    //Next, we figure out if we can exploit things like iprev == -1 --> jprev == -1
    bool simple_mode = true;
    if ( (ip2 == -1   && jp2 != -1)   || (ip2 != -1   && jp2 == -1) ||
         (iprev == -1 && jprev != -1) || (iprev != -1 && jprev == -1) ||
         (inext == -1 && jnext != -1) || (inext != -1 && jnext == -1) ||
         (in2 == -1   && jn2 != -1)   || (in2 != -1   && jn2 == -1) ) simple_mode = false;

    //dihedral set overlap?
    bool overlapA = false;
    bool overlapB = false;
    if (jprev != -1 && jprev == (int) iTag)             overlapA = true;
    if (jnext != -1 && jnext == (int) swap_tag_i)       overlapB = true;
    //If either of these flags are true, a swap is happening in the middle of a chain,
    //changing the logic a bit.

    std::vector<unsigned int> new_dih(4);
    if (simple_mode && !overlapA && !overlapB)
        {
        if (ip2 != -1)
            {
            new_dih[0]=ip2; new_dih[1]=iprev;new_dih[2]=iTag;new_dih[3]=swap_tag_i;
            initial_dihedrals.push_back(new_dih);
            new_dih[3] = swap_tag_j;
            final_dihedrals.push_back(new_dih);
            new_dih[0]=jp2; new_dih[1]=jprev;new_dih[2]=jTag;new_dih[3]=swap_tag_j;
            initial_dihedrals.push_back(new_dih);
            new_dih[3] = swap_tag_i;
            final_dihedrals.push_back(new_dih);
            };
        if(iprev != -1 && inext != -1)
            {
            new_dih[0]=iprev;new_dih[1]=iTag;new_dih[2]=swap_tag_i; new_dih[3]=inext;
            initial_dihedrals.push_back(new_dih);
            new_dih[2] = swap_tag_j;new_dih[3]=jnext;
            final_dihedrals.push_back(new_dih);
            new_dih[0]=jprev;new_dih[1]=jTag;new_dih[2]=swap_tag_j; new_dih[3]=jnext;
            initial_dihedrals.push_back(new_dih);
            new_dih[2] = swap_tag_i;new_dih[3]=inext;
            final_dihedrals.push_back(new_dih);
            };
        if(in2 != -1)
            {
            new_dih[0]=iTag;new_dih[1]=swap_tag_i; new_dih[2]=inext;new_dih[3]=in2;
            initial_dihedrals.push_back(new_dih);
            new_dih[1]=swap_tag_j;new_dih[2]=jnext;new_dih[3]=jn2;
            final_dihedrals.push_back(new_dih);
            new_dih[0]=jTag;new_dih[1]=swap_tag_j; new_dih[2]=jnext;new_dih[3]=jn2;
            initial_dihedrals.push_back(new_dih);
            new_dih[1]=swap_tag_i;new_dih[2]=inext;new_dih[3]=in2;
            final_dihedrals.push_back(new_dih);
            };
        };
    if (simple_mode && overlapA)
        {
        if (jn2 != -1)
            {
            new_dih[0]=jTag;new_dih[1]=swap_tag_j; new_dih[2]=jnext;new_dih[3]=jn2;
            initial_dihedrals.push_back(new_dih);
            new_dih[0]=iTag;
            final_dihedrals.push_back(new_dih);
            new_dih[0]=iTag;new_dih[1]=swap_tag_i; new_dih[2]=inext;new_dih[3]=in2;
            initial_dihedrals.push_back(new_dih);
            new_dih[0]=jTag;
            final_dihedrals.push_back(new_dih);
            };
        if (jnext != -1)
            {
            new_dih[0]=iTag;new_dih[1]=jTag; new_dih[2]=swap_tag_j;new_dih[3]=jnext;
            initial_dihedrals.push_back(new_dih);
            new_dih[0]=jTag;new_dih[1]=iTag;
            final_dihedrals.push_back(new_dih);
            new_dih[0]=jTag;new_dih[1]=iTag; new_dih[2]=swap_tag_i;new_dih[3]=inext;
            initial_dihedrals.push_back(new_dih);
            new_dih[0]=iTag;new_dih[1]=jTag;
            final_dihedrals.push_back(new_dih);
            };
        new_dih[0] = swap_tag_i;new_dih[1]=iTag;new_dih[2]=jTag;new_dih[3]=swap_tag_j;
        initial_dihedrals.push_back(new_dih);
        new_dih[1] = jTag; new_dih[2]=iTag;
        final_dihedrals.push_back(new_dih);
        };

    if (simple_mode && overlapB)
        {
        if (jp2 != -1)
            {
            new_dih[0]=jp2;new_dih[1]=jprev; new_dih[2]=jTag;new_dih[3]=swap_tag_j;
            initial_dihedrals.push_back(new_dih);
            new_dih[3]=swap_tag_i;
            final_dihedrals.push_back(new_dih);
            new_dih[0]=ip2;new_dih[1]=iprev; new_dih[2]=iTag;new_dih[3]=swap_tag_i;
            initial_dihedrals.push_back(new_dih);
            new_dih[3]=swap_tag_j;
            final_dihedrals.push_back(new_dih);
            };
        if (jprev != -1)
            {
            new_dih[0]=jprev;new_dih[1]=jTag; new_dih[2]=swap_tag_j;new_dih[3]=swap_tag_i;
            initial_dihedrals.push_back(new_dih);
            new_dih[2]=swap_tag_i;new_dih[3]=swap_tag_j;
            final_dihedrals.push_back(new_dih);
            new_dih[0]=iprev;new_dih[1]=iTag; new_dih[2]=swap_tag_i;new_dih[3]=swap_tag_j;
            initial_dihedrals.push_back(new_dih);
            new_dih[2]=swap_tag_j;new_dih[3]=swap_tag_i;
            final_dihedrals.push_back(new_dih);
            };
        new_dih[0] = jTag;new_dih[1]=swap_tag_j;new_dih[2]=swap_tag_i;new_dih[3]=iTag;
        initial_dihedrals.push_back(new_dih);
        new_dih[1] = swap_tag_i; new_dih[2]=swap_tag_j;
        final_dihedrals.push_back(new_dih);
        };


    if (!simple_mode)
        {
        m_exec_conf->msg->error() << "findNearbyDihedrals has entered simple_mode == false."
             << " This could cause the number of dihedrals in system to change, a currently"
             << " unsupported branch of the code"  <<std::endl;
        throw runtime_error("BondSwap::findNearbyDihedrals accessed in an unsupported mode");
        //this isn't going to be pretty.
        //For different possible sets of ip2-ip1-iT-is-in1-in2 and jp2-jp1-jT-js-jn1-jn2 (i.e. with some negative ones
        //in there) one either will or will not have to add new and or delete existing dihedrals.
        //To procede, one can either (a) repeat the explicit enumeration of all possibilities (as I have done in the
        //NearbyAngle function)   or (b) mark \a every existing dihedral for deletion, and then add in the new ones.
        //
        //Since (!simple_mode) implies that safety_mode should be on, where we are already sacrificing
        //performance here I will follow option (b).
        setSafetyMode(true);
        //cout << "not in simple mode for dihedrals. How unforuntate" << endl;
        //cout << ip2 <<"*"<<iprev <<"*" <<iTag << "*" << swap_tag_i << "*" << inext << "*" << in2 << endl;
        //cout << jp2 <<"*"<<jprev <<"*" <<jTag << "*" << swap_tag_j << "*" << jnext << "*" << jn2 << endl;
        };

    };//end of function

/*! \param bond_table vector of vector of tags.
    \post bonded[i] will hold a vector of length 3*(number of particles bonded to tag i),
    with entries arranged as (particle tag bonded to i, chain_pos of that particle tag, bond index)
*/
void BondSwap::buildBondTable(std::vector<std::vector<unsigned int> >  &bonded)
    {

    std::vector<std::vector<unsigned int> > bond_table(m_pdata->getNGlobal());
    ArrayHandle<unsigned int> h_chain_position(m_chain_position,access_location::host, access_mode::read);

    //get bondData from snapshot
    boost::shared_ptr<BondData> initialBonds = m_sysdef->getBondData();
    BondData::Snapshot snapshot(initialBonds->getNGlobal());
    initialBonds->takeSnapshot(snapshot);
    //broadcast global bond list
    std::vector<BondData::members_t> bonds;
#ifdef ENABLE_MPI
    if (m_pdata->getDomainDecomposition())
        {
        if (m_exec_conf->getRank()==0)
            bonds=snapshot.groups;
        bcast(bonds,0,m_exec_conf->getMPICommunicator());
        }
    else
#endif
        {
        bonds=snapshot.groups;
        };

    unsigned int bsize = bonds.size();
    for(unsigned int bb = 0; bb < bsize; ++bb)
        {
        const BondData::members_t& bond = bonds[bb];
        unsigned int p1 = bond.tag[0];
        unsigned int cp1 = h_chain_position.data[p1];
        unsigned int p2 = bond.tag[1];
        unsigned int cp2 = h_chain_position.data[p2];
            bond_table[p1].push_back(p2); bond_table[p1].push_back(cp2);bond_table[p1].push_back(bb);
            bond_table[p2].push_back(p1); bond_table[p2].push_back(cp1);bond_table[p2].push_back(bb);
        };
    bonded=bond_table;
    m_exec_conf->msg->notice(10) << "BondSwap Bond Table (re)built" << endl;

    };

/*! \param angle_table vector of vector of vector of tags.
    \post every vector of angle tags (with an entry for the angle index [a1,a2,a3,angle_idx]
            will be in each of the vectors angle_table[a1], angle_table[a2], and angle_table[a3]
*/
void BondSwap::buildAngleTable(std::vector< std::vector<std::vector<unsigned int> > > &angle_table)
    {
    std::vector< std::vector<std::vector<unsigned int> > > new_ang_table;
    new_ang_table.resize(m_pdata->getNGlobal());
    boost::shared_ptr<AngleData> angle_data = m_sysdef->getAngleData();
    // access angle data by snapshot
    AngleData::Snapshot snapshot(angle_data->getNGlobal());
    angle_data->takeSnapshot(snapshot);
    // broadcast global angle list
    std::vector<AngleData::members_t> angles;

#ifdef ENABLE_MPI
    if (m_pdata->getDomainDecomposition())
        {
        if (m_exec_conf->getRank() == 0)
            angles = snapshot.groups;
        bcast(angles, 0, m_exec_conf->getMPICommunicator());
        }
    else
#endif
        {
        angles = snapshot.groups;
        };

    // for each angle
    for (unsigned int i = 0; i < angles.size(); i++)
        {
        std::vector<unsigned int> angle_triple(4);
        unsigned int a1 = angles[i].tag[0];
        unsigned int a2 = angles[i].tag[1];
        unsigned int a3 = angles[i].tag[2];
        angle_triple[0]=a1;angle_triple[1]=a2;angle_triple[2]=a3;angle_triple[3]=i;
        new_ang_table[a1].push_back(angle_triple);
        new_ang_table[a2].push_back(angle_triple);
        new_ang_table[a3].push_back(angle_triple);
        }
    angle_table = new_ang_table;
    m_exec_conf->msg->notice(10) << "BondSwap angle Table (re)built" << endl;

    };

/*! \param dihedral_table vector of vector of vector of tags.
    \post every vector of dihedral tags (with an entry for the dihedral index [d1,d2,d3,d4,dihedral_idx]
    will be in each of the vectors
    dihedral_table[d1], dihedral_table[d2], dihedral_table[d3], and dihedral_table[d4]
*/
void BondSwap::buildDihedralTable(std::vector<std::vector<std::vector<unsigned int> > > &dihedral_table)
    {
    std::vector<std::vector<std::vector<unsigned int> > > new_dih_table;
    new_dih_table.resize(m_pdata->getNGlobal());
    boost::shared_ptr<DihedralData> dihedral_data = m_sysdef->getDihedralData();
    // access angle data by snapshot
    DihedralData::Snapshot snapshot(dihedral_data->getNGlobal());
    dihedral_data->takeSnapshot(snapshot);
    // broadcast global angle list
    std::vector<DihedralData::members_t> dihedrals;

#ifdef ENABLE_MPI
    if (m_pdata->getDomainDecomposition())
        {
        if (m_exec_conf->getRank() == 0)
            dihedrals = snapshot.groups;
        bcast(dihedrals, 0, m_exec_conf->getMPICommunicator());
        }
    else
#endif
        {
        dihedrals = snapshot.groups;
        };

    // for each angle
    for (unsigned int i = 0; i < dihedrals.size(); i++)
        {
        std::vector<unsigned int> dihedral_quad(5);
        unsigned int d1 = dihedrals[i].tag[0];
        unsigned int d2 = dihedrals[i].tag[1];
        unsigned int d3 = dihedrals[i].tag[2];
        unsigned int d4 = dihedrals[i].tag[3];
        dihedral_quad[0]=d1;dihedral_quad[1]=d2;dihedral_quad[2]=d3;dihedral_quad[3]=d4;dihedral_quad[4]=i;
        new_dih_table[d1].push_back(dihedral_quad);
        new_dih_table[d2].push_back(dihedral_quad);
        new_dih_table[d3].push_back(dihedral_quad);
        new_dih_table[d4].push_back(dihedral_quad);
        }
    dihedral_table=new_dih_table;
    m_exec_conf->msg->notice(10) << "BondSwap dihedral Table (re)built" << endl;
    };
/*! \post bond, angle, and dihedral auxiliary tables will be built
*/
void BondSwap::buildTables()
    {
    buildBondTable(m_bond_table);
    buildAngleTable(m_angle_table);
    buildDihedralTable(m_dihedral_table);
    m_exec_conf->msg->notice(8) << "BondSwap auxiliary tables built" << std::endl;
    if (m_pdata->getDomainDecomposition())
        {
        bcast(m_bond_table,0,m_exec_conf->getMPICommunicator());
        bcast(m_angle_table,0,m_exec_conf->getMPICommunicator());
        bcast(m_dihedral_table,0,m_exec_conf->getMPICommunicator());
        }

    };
/*! \param positions a python list of unsigned integers
    \post the current values of m_chain_position GPUArray will be completely overridden with the values
    in the python list. The list must contain an entry for every particle in the system.
*/
void BondSwap::setChainPositions(boost::python::list positions)
    {
    bool correct_length = false;
    unsigned int pNum = m_pdata->getNGlobal();
    if(len(positions) == pNum) correct_length=true;
    if(!correct_length)
        {
        m_exec_conf->msg->error() << "Set an incorrect number of chain positions"  <<std::endl;
        throw runtime_error("BondSwap::setChainPositions set an incorrect number of positions");
        };
    ArrayHandle<unsigned int> h_chain_position(m_chain_position,access_location::host, access_mode::overwrite);
    for (unsigned int i = 0; i < len(positions); ++i)
        {
        unsigned int new_pos = extract<unsigned int>(positions[i]);
        h_chain_position.data[i] = new_pos;
        };
    m_exec_conf->msg->notice(5) << "new chain positions set in the BondSwap updater" << std::endl;
    };

void BondSwap::setSafetyMode(bool safety)
    {
    if (m_safety_mode != safety)
        {
        m_safety_mode = safety;
        if (safety)
            m_exec_conf->msg->notice(4) << "BondSwap safety mode set to true" << std::endl;
        if (!safety)
            m_exec_conf->msg->notice(4) << "BondSwap safety mode set to false" << std::endl;
        };
    };

void BondSwap::printBondSwapStats()
    {
    ArrayHandle<int> h_mc_stats(m_mc_stats,access_location::host, access_mode::read);
    if(h_mc_stats.data[0]>0)
        {
        Scalar frac = (Scalar)(h_mc_stats.data[1])/(Scalar)(h_mc_stats.data[0]);
        cout << "Bond swapping statistics:" << endl;
        cout << "--------------------" << endl;
        cout <<"Attempted: " << h_mc_stats.data[0] << "\t Accepted: " << h_mc_stats.data[1] << std::endl;
        cout << "Fractional acceptance: " << frac
             <<std::endl << "--------------------" << std::endl;
        }
    else
        {
        cout << "Bond swapping statistics:" << endl;
        cout << "--------------------" << endl;
        cout <<"No bond swaps attempted"
             <<std::endl << "--------------------" << std::endl;
        };
    }


void export_BondSwap()
    {
    class_<BondSwap, boost::shared_ptr<BondSwap>, bases<Updater>, boost::noncopyable>
    ("BondSwap", init< boost::shared_ptr<SystemDefinition>,boost::shared_ptr<NeighborList>,
    Scalar,Scalar,Scalar, int, int >())
    .def("setSafetyMode",&BondSwap::setSafetyMode)
    .def("buildTables",&BondSwap::buildTables)
    .def("addForceCompute",&BondSwap::addForceCompute)
    .def("setAngleExclusions",&BondSwap::setAngleExclusions)
    .def("setDihedralExclusions",&BondSwap::setDihedralExclusions)
    .def("setMCTemperature",&BondSwap::setMCTemperature)
    .def("setChainPositions",&BondSwap::setChainPositions)
    .def("printBondSwapStats",&BondSwap::printBondSwapStats)
    ;
    }

