/*
Highly Optimized Object-oriented Many-particle Dynamics -- Blue Edition
(HOOMD-blue) Open Source Software License Copyright 2009-2015 The Regents of
the University of Michigan All rights reserved.

HOOMD-blue may contain modifications ("Contributions") provided, and to which
copyright is held, by various Contributors who have granted The Regents of the
University of Michigan the right to modify and/or distribute such Contributions.

You may redistribute, use, and create derivate works of HOOMD-blue, in source
and binary forms, provided you abide by the following conditions:

* Redistributions of source code must retain the above copyright notice, this
list of conditions, and the following disclaimer both in the code and
prominently in any materials provided with the distribution.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions, and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* All publications and presentations based on HOOMD-blue, including any reports
or published results obtained, in whole or in part, with HOOMD-blue, will
acknowledge its use according to the terms posted at the time of submission on:
http://codeblue.umich.edu/hoomd-blue/citations.html

* Any electronic documents citing HOOMD-Blue will link to the HOOMD-Blue website:
http://codeblue.umich.edu/hoomd-blue/

* Apart from the above required attributions, neither the name of the copyright
holder nor the names of HOOMD-blue's contributors may be used to endorse or
promote products derived from this software without specific prior written
permission.

Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND/OR ANY
WARRANTIES THAT THIS SOFTWARE IS FREE OF INFRINGEMENT ARE DISCLAIMED.

IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*! \file BondSwapGPU.cc
    \brief Defines the BondSwapGPU class
*/

#include "BondSwapGPU.h"
#include "BondSwapGPU.cuh"


using namespace std;
using namespace boost::python;

/*! \param sysdef System from which to compute
    \param nlist NeighborList to use for proposing swap candidates
    \param fraction Fraction of particles to check for swaps
    \param distSquared Maximum squared distance between swap candidates
    \param Tnew Temperature to use for Monte Carlo acceptance
    \param seed Seed to use (along with the timestep) for saru RNG
    \param period Period at which updater will run
*/
BondSwapGPU::BondSwapGPU(boost::shared_ptr<SystemDefinition> sysdef,
                boost::shared_ptr<NeighborList> nlist,
                Scalar fraction,
                Scalar distSquared,
                Scalar Tnew,
                int seed,
                int period)
    : BondSwap(sysdef,nlist,fraction,distSquared,Tnew,seed,period)
    {
    // at least one GPU is needed
    if (!m_exec_conf->isCUDAEnabled())
        {
        m_exec_conf->msg->error() << "Creating a BondSwapGPU with no GPU in the execution configuration" << std::endl;
        throw std::runtime_error("Error initializing BondSwapGPU");
        }

    //set up the autotuner and initial values for the flat bond table
    m_tuner_swapset.reset(new Autotuner(32, 1024, 32, 5, 200000, "bondswap_swapset", this->m_exec_conf));
    m_max_bond_N = 0;
    m_rebuild_bond_table = true;
    m_exec_conf->msg->notice(5) << "Constructing BondSwapGPU updater"<< std::endl;

    };


void BondSwapGPU::flattenBondTable()
    {
    //This mechanism flattens the m_bond_table structure so it can be easily
    //communicated to the GPU
    m_max_bond_N =0;
    for (unsigned int k = 0; k < m_bond_table.size(); ++k)
        if (m_bond_table[k].size() > m_max_bond_N) m_max_bond_N=m_bond_table[k].size();

    m_b_table.resize(m_pdata->getNGlobal()*m_max_bond_N);
    std::fill(m_b_table.begin(),m_b_table.end(),-1);
    for (unsigned int k = 0; k < m_bond_table.size(); ++k)
        for (unsigned int k2 = 0; k2 < m_bond_table[k].size(); ++k2)
            m_b_table[k*m_max_bond_N+k2] = m_bond_table[k][k2];
    m_rebuild_bond_table = false;
    };


/*! \param quads a vector to hold potential swap sets
    \param timestep current timestep of the simulation
*/
void BondSwapGPU::findPotentialSwaps(std::vector< std::vector<int> > &quads, int timestep)
    {
    if (m_prof) m_prof->push("findSwaps");

    //This routine is conceptually identical to the one in the CPU version of the BondSwap updater.
    //The only difference is that here the task of finding potential swap sets is farmed out to the GPU

    //first, we set up an auxiliary structure to communicate the results back from the GPU
    unsigned int maxN = m_pdata->getNGlobal();
    std::vector<int> goodNeighs(maxN*3,-1);


    const BoxDim& box = m_pdata->getGlobalBox();
    ArrayHandle<Scalar4> d_pos(m_pdata->getPositions(), access_location::device, access_mode::read);
    ArrayHandle<unsigned int> d_tag(m_pdata->getTags(), access_location::device, access_mode::read);
    ArrayHandle<unsigned int> d_Rtag(m_pdata->getRTags(), access_location::device, access_mode::read);
    ArrayHandle<unsigned int> d_chain_position(m_chain_position,access_location::device, access_mode::read);
    ArrayHandle<unsigned int> d_n_neigh(m_nlist->getNNeighArray(),access_location::device,access_mode::read);
    ArrayHandle<unsigned int> d_nlist(m_nlist->getNListArray(),access_location::device,access_mode::read);
    ArrayHandle<unsigned int> d_head_list(m_nlist->getHeadList(),access_location::device,access_mode::read);

    if (m_prof) m_prof->push("gpuKernelSearch");
    // call a GPU kernel to find (iTag,jTag,in,jn) sets
    // that are nearby and share the same m_chain_position label
    m_tuner_swapset->begin();
    gpu_find_bond_swap_candidate_one(d_pos.data,d_tag.data,d_Rtag.data,d_chain_position.data,box,d_n_neigh.data,
                                     d_nlist.data,d_head_list.data, maxN,&goodNeighs[0],m_max_bond_N,&m_b_table[0],
                                     m_distSquared,m_tuner_swapset->getParam());
    m_tuner_swapset->end();
    if (m_prof) m_prof->pop();

    //scan results of the gpu calculation for potential swaps. If found, put them in the form expected by the CPU code
    if (m_prof) m_prof->push("swapSearch");
    Saru saru(m_seed,timestep);
    std::vector<int> quad(7,-1);
    int swaps_found = 0;
    unsigned int iTag = 0;
    for (unsigned int ii = 0; ii < goodNeighs.size(); ii = ii+3)
    //for (unsigned int ii = 0, size = goodNeighs.size(); ii< size;ii = ii+3)
        {
        //we only want to consider a fraction of the particles for swapping:
        if (saru.f(0,1) > m_fraction)
            {
            iTag+=1;
            continue;
            };
        if (goodNeighs[ii] >=0)
            {
            unsigned int jTag = goodNeighs[ii];
            unsigned int swap_tag_i = goodNeighs[ii+1];
            unsigned int swap_tag_j = goodNeighs[ii+2];

            unsigned int b1_idx = 0;
            unsigned int b2_idx = 0;
            for (unsigned int b1 = 0; b1 < m_bond_table[iTag].size(); b1 = b1 +3)
                if(swap_tag_i == m_bond_table[iTag][b1]) b1_idx = m_bond_table[iTag][b1+2];
            for (unsigned int b2 = 0; b2 < m_bond_table[jTag].size(); b2 = b2 +3)
                if(swap_tag_j == m_bond_table[jTag][b2]) b2_idx = m_bond_table[jTag][b2+2];


            quad[0]=iTag;quad[1]=swap_tag_i;
            quad[2]=jTag;quad[3]=swap_tag_j;
            quad[4]=b1_idx;quad[5] = b2_idx;
            quads.push_back(quad);
            swaps_found +=1;
            };
        iTag += 1;
        };
    if (m_prof) m_prof->pop();

    //in the main update loop quads should have at least one element... if empty feed it a junk set
    if (swaps_found == 0 ) quads.push_back(quad);
    if (m_prof) m_prof->pop();
    }


/*!
    This is mostly identical to the CPU version, but with minor tweaks for the bond table handling.
    Please see BondSwap.cc for comments on much of this code
    \param timestep Current time step of the simulation
*/
void BondSwapGPU::update(unsigned int timestep)
    {
    if (m_prof) m_prof->push("BondSwap");

    unsigned int local_rank = m_exec_conf->getRank();
// m_nlist->compute(timestep);
    std::vector<std::vector<int> > quads_single;

    if (m_prof) m_prof->push("FlatTableHandling");
    if(m_rebuild_bond_table)
        flattenBondTable();
    if (m_prof) m_prof->pop();

    //Step (1)
    findPotentialSwaps(quads_single,timestep);
    std::vector<std::vector<int> > quads;
        {
        quads=quads_single;
        };

    //Step (2)
    if (m_prof) m_prof->push("attemptSwap");
    std::vector<std::vector<std::vector<std::vector<unsigned int> > > >angle_change_all;
    std::vector<std::vector<std::vector<std::vector<unsigned int> > > >dihedral_change_all;
    for (unsigned int qs = 0; qs  < quads.size(); ++qs)
        {
        std::vector<std::vector<std::vector<unsigned int> > > angle_change_local;
        std::vector<std::vector<std::vector<unsigned int> > > dihedral_change_local;
        bool success = attemptSwap(quads[qs],timestep,m_bond_table,m_angle_table,m_dihedral_table,
                                   angle_change_local, dihedral_change_local);
        angle_change_all.push_back(angle_change_local);
        dihedral_change_all.push_back(dihedral_change_local);
        if(success) quads[qs][6] = 1;
        };
    if (m_prof) m_prof->pop();


    //Step (3) if any swaps were found to be good
    if (m_prof) m_prof->push("ChangeLoop");
    bool accept_any = false;
    int attempt_num = 0;
    int accept_num = 0;
    std::vector<std::vector<std::vector<unsigned int> > > angle_change;
    std::vector<std::vector<std::vector<unsigned int> > > dihedral_change;
    std::vector<std::vector<unsigned int> > accepted_sets;
    unsigned int iTag,jTag,swap_tag_i,swap_tag_j,b1_idx,b2_idx;
    for (unsigned int qs = 0; qs < quads.size(); ++qs)
        {
        std::vector<int> quad = quads[qs];
        if(quad[0] > 0)
            attempt_num += 1;
        if (quad.size() > 6 && quad[6] == 1)
            {
            accept_any=true;
            m_rebuild_bond_table = true;
            //Step (3)
            iTag = quad[0];swap_tag_i = quad[1];
            jTag = quad[2];swap_tag_j = quad[3];
            b1_idx = quad[4]; b2_idx = quad[5];
            bool already_modified = false;
            for (unsigned int cc = 0; cc < accepted_sets.size(); ++cc)
                {
                std::vector<unsigned int> cur_set = accepted_sets[cc];
                for (unsigned int ll = 0; ll < cur_set.size(); ++ll)
                    if(iTag==cur_set[ll]||jTag==cur_set[ll]||swap_tag_i==cur_set[ll]||swap_tag_j==cur_set[ll])
                        already_modified = true;
                };
            if(already_modified)
                {
                attempt_num -= 1;
                continue;
                };
            accept_num += 1;
            std::vector<unsigned int> good_set(4); good_set[0]=iTag;
            good_set[1]=jTag; good_set[2]=swap_tag_i; good_set[3]=swap_tag_j;
            accepted_sets.push_back(good_set);


            //change bonded interaction pairs
            changeBondByTag(iTag,swap_tag_i,iTag,swap_tag_j);
            changeBondByTag(jTag,swap_tag_j,jTag,swap_tag_i);
            m_exec_conf->msg->notice(5) << "changed the " << iTag <<"-" <<swap_tag_i << "  and " <<  jTag <<"-"
                <<swap_tag_j << " bonds on rank " << local_rank <<  " at timestep " << timestep <<  endl;

            if (!m_safety_mode)
                {
                //swap the bond exclusions
                m_nlist->swapExclusion(iTag,swap_tag_i,jTag,swap_tag_j);
                //Update the auxiliary bond table
                for (unsigned int bb = 0; bb < m_bond_table[iTag].size()/3; ++bb)
                    if(m_bond_table[iTag][3*bb] == swap_tag_i && m_bond_table[iTag][3*bb+2]==b1_idx)
                        m_bond_table[iTag][3*bb] = swap_tag_j;
                for (unsigned int bb = 0; bb < m_bond_table[swap_tag_i].size()/3; ++bb)
                    if(m_bond_table[swap_tag_i][3*bb] == iTag && m_bond_table[swap_tag_i][3*bb+2]==b1_idx)
                        {m_bond_table[swap_tag_i][3*bb] = jTag;m_bond_table[swap_tag_i][3*bb+2] = b2_idx;};
                for (unsigned int bb = 0; bb < m_bond_table[jTag].size()/3; ++bb)
                    if(m_bond_table[jTag][3*bb] == swap_tag_j && m_bond_table[jTag][3*bb+2]==b2_idx)
                        m_bond_table[jTag][3*bb] = swap_tag_i;
                for (unsigned int bb = 0; bb < m_bond_table[swap_tag_j].size()/3; ++bb)
                    if(m_bond_table[swap_tag_j][3*bb] == jTag && m_bond_table[swap_tag_j][3*bb+2]==b2_idx)
                        {m_bond_table[swap_tag_j][3*bb] = iTag; m_bond_table[swap_tag_j][3*bb+2] = b1_idx;};
                };


            //If needed, swap angles
            angle_change = angle_change_all[qs];
            if(m_check_angle && angle_change.size() == 2)
                {
                for(unsigned int ai = 0; ai < angle_change[0].size(); ++ai)
                    {
                    std::vector<unsigned int> ang(3);
                    std::vector<unsigned int> ang2(3);
                    ang[0] = angle_change[0][ai][0];
                    ang[1] = angle_change[0][ai][1];
                    ang[2] = angle_change[0][ai][2];
                    ang2[0] = angle_change[1][ai][0];
                    ang2[1] = angle_change[1][ai][1];
                    ang2[2] = angle_change[1][ai][2];
                    changeAngleByTag(ang[0],ang[1],ang[2],ang2[0],ang2[1],ang2[2]);
                    m_exec_conf->msg->notice(5)  << "changed the " <<ang[0]<<"-"<<ang[1]<<"-"<<ang[2] << " angle to "
                                                <<ang2[0]<<"-"<<ang2[1]<<"-"<<ang2[2]  <<std::endl;

                    if(!m_safety_mode)
                        {
                        //change the m_angle_table entries as appropriate...
                        for (unsigned int aj = 0; aj < 3 ; ++aj)
                            {
                            unsigned int id1 = ang[aj];
                            std::vector<unsigned int> old_ang(4,0);
                            std::vector<unsigned int> new_ang(4,0);
                            new_ang[0]=ang2[0]; new_ang[1]=ang2[1]; new_ang[2]=ang2[2];
                            std::vector< std::vector<unsigned int > > new_table_entry;
                            for (unsigned int aa=0; aa < m_angle_table[id1].size(); ++aa)
                                {
                                old_ang = m_angle_table[id1][aa];
                                //if old_ang is not the angle to be replaced,
                                //add it to new_table_entry, otherwise note the index
                                bool ang_select = (old_ang[0]==ang[0]&&old_ang[1]==ang[1]&&old_ang[2]==ang[2])||
                                                  (old_ang[2]==ang[0]&&old_ang[1]==ang[1]&&old_ang[0]==ang[2]);
                                if (ang_select)
                                    {
                                    new_ang[3] = old_ang[3];
                                    }
                                else
                                    {
                                    new_table_entry.push_back(old_ang);
                                    };
                                };
                            if(id1 == ang2[0] || id1==ang2[1] || id1==ang2[2])
                                new_table_entry.push_back(new_ang);
                            m_angle_table[id1] = new_table_entry;
                            //finally, make sure the new angle appears in the spots for the final state of the angle
                            if(id1 !=  ang2[0] && id1 != ang2[1] && id1!=ang2[2])
                                m_angle_table[ang2[aj]].push_back(new_ang);
                            };
                        //update angular exclusions if necessary
                        //default HOOMD behavior is not to exclude the 1-3 or 1-4 interactions...
                        if(m_excl_angle)
                            {
                            for (unsigned int ai = 0; ai < angle_change[0].size()/2; ++ai)
                                {
                                unsigned int a_idx = 2*ai;
                                unsigned int ai1,ai2,aj1,aj2;
                                ai1 = angle_change[0][a_idx][0];
                                ai2 = angle_change[0][a_idx][2];
                                aj1 = angle_change[0][a_idx+1][0];
                                aj2 = angle_change[0][a_idx+1][2];
                                m_nlist->swapExclusion(ai1,ai2,aj1,aj2);
                                };
                            };
                        };
                    };
                };
            //If needed, swap dihedrals
            dihedral_change = dihedral_change_all[qs];
            if(m_check_dihedral && dihedral_change.size()==2&&dihedral_change[0].size()==dihedral_change[1].size())
                {
                for(unsigned int di = 0; di < dihedral_change[0].size(); ++di)
                    {
                    std::vector<unsigned int> dihed(4);
                    std::vector<unsigned int> dihed2(3);
                    dihed[0] = dihedral_change[0][di][0];
                    dihed[1] = dihedral_change[0][di][1];
                    dihed[2] = dihedral_change[0][di][2];
                    dihed[3] = dihedral_change[0][di][3];
                    dihed2[0] = dihedral_change[1][di][0];
                    dihed2[1] = dihedral_change[1][di][1];
                    dihed2[2] = dihedral_change[1][di][2];
                    dihed2[3] = dihedral_change[1][di][3];
                    changeDihedralByTag(dihed[0],dihed[1],dihed[2],dihed[3],dihed2[0],dihed2[1],dihed2[2],dihed2[3]);
                    m_exec_conf->msg->notice(5) << "swap "<<dihed[0]<<"-"<<dihed[1]<<"-"<<dihed[2] <<"-"<<dihed[3]
                         << " dihedral with "<< dihed2[0]<<"-"<<dihed2[1]<<"-"<<dihed2[2]<<"-"<<dihed2[3]<<std::endl;

                    if (!m_safety_mode)
                        {
                        //change the m_dihedral_table entries as appropriate...
                        for (unsigned int aj = 0; aj < 4 ; ++aj)
                            {
                            unsigned int id1 = dihed[aj];
                            std::vector<unsigned int> old_dih(5,0);
                            std::vector<unsigned int> new_dih(5,0);
                            new_dih[0]=dihed2[0]; new_dih[1]=dihed2[1]; new_dih[2]=dihed2[2];new_dih[3]=dihed2[3];
                            std::vector< std::vector<unsigned int > > new_table_entry;
                            for (unsigned int aa=0; aa < m_dihedral_table[id1].size(); ++aa)
                                {
                                old_dih = m_dihedral_table[id1][aa];
                                //if old_dih is not the dihedral to be replaced,
                                //add it to new_table_entry, otherwise note the index
                                bool dih_select = (old_dih[0]==dihed[0]&&old_dih[1]==dihed[1]&&
                                                   old_dih[2]==dihed[2]&&old_dih[3]==dihed[3]) ||
                                                  (old_dih[3]==dihed[0]&&old_dih[2]==dihed[1]&&
                                                   old_dih[1]==dihed[2]&&old_dih[0]==dihed[3]);
                                if (dih_select)
                                    {
                                    new_dih[4] = old_dih[4];
                                    }
                                else
                                    {
                                    new_table_entry.push_back(old_dih);
                                    };
                                };
                            if(id1 == dihed2[0] || id1==dihed2[1] || id1==dihed2[2] || id1 == dihed2[3])
                                new_table_entry.push_back(new_dih);
                            m_dihedral_table[id1] = new_table_entry;
                            //finally, make sure the new dihedral appears in the spots for the final state of the
                            //dihedral but note (see below) that the last dihedral_change element may be a purely
                            //internal dihedral rearrangement
                            bool last_dih = true;
                            if (di+1 == dihedral_change[0].size())
                                if( dihedral_change[0].size() % 2  ==1 && dihedral_change[0].size() == 5 )
                                    last_dih=false;
                            if(id1 !=  dihed2[0]&&id1!=dihed2[1]&&id1!=dihed2[2]&&id1!=dihed2[3] && last_dih)
                                m_dihedral_table[dihed2[aj]].push_back(new_dih);
                            };
                        //update dihedral exclusions if necessary
                        if(m_excl_dihedral)
                            {
                            //there can be an odd number of dihedral swaps at the center of a chain (depending on
                            //the labeling). In that case, the very last dihedral swap only rearranges internal
                            //monomers (enforced by the findNearbyDihedrals function), so no exclusions need to
                            //be changed
                            for (unsigned int di = 0; di < dihedral_change[0].size()/2; ++di)
                                {
                                unsigned int d_idx = 2*di;
                                if (dihedral_change[0].size() > d_idx+1)
                                    {
                                    unsigned int di1,di2,dj1,dj2;
                                    di1 = dihedral_change[0][d_idx][0];
                                    di2 = dihedral_change[0][d_idx][3];
                                    dj1 = dihedral_change[0][d_idx+1][0];
                                    dj2 = dihedral_change[0][d_idx+1][3];
                                    m_nlist->swapExclusion(di1,di2,dj1,dj2);
                                    };
                                };
                            };
                        };
                    };
                };//end dihedral if
                if(m_check_angle || m_check_dihedral)
                    break;
            }; //this is attached to the loop over possible swap sets
        };
    ArrayHandle<int> h_mc_stats(m_mc_stats,access_location::host, access_mode::readwrite);
    h_mc_stats.data[0]=h_mc_stats.data[0] + attempt_num;
    h_mc_stats.data[1]=h_mc_stats.data[1] + accept_num;

    if (m_prof) m_prof->pop(); //profiler for the change loop

    if (m_prof) m_prof->push("RecomputeNlistForces");
    if(accept_any)
        {
        m_sysdef->getBondData()->setDirty();
        if(m_safety_mode)
            {
            m_nlist->clearExclusions();
            m_nlist->addExclusionsFromBonds();
            buildBondTable(m_bond_table);
            };
        if(m_check_angle)
            {
            if(m_safety_mode)
                {
                m_nlist->addExclusionsFromAngles();
                buildAngleTable(m_angle_table);
                };
            m_sysdef->getAngleData()->setDirty();
            };
        if(m_check_dihedral)
            {
            if(m_safety_mode)
                {
                m_nlist->addExclusionsFromDihedrals();
                buildDihedralTable(m_dihedral_table);
                };
            m_sysdef->getDihedralData()->setDirty();
           };

        //flag the neighbor list to be updated
        m_nlist->forceUpdate();

        }

    if (m_prof) m_prof->pop(); //profiler for the rebuild stage

    if (m_prof) m_prof->pop(); //profiler for the whole class
    }





void export_BondSwapGPU()
    {
    class_<BondSwapGPU, boost::shared_ptr<BondSwapGPU>, bases<BondSwap>, boost::noncopyable>
    ("BondSwapGPU", init< boost::shared_ptr<SystemDefinition>,boost::shared_ptr<NeighborList>,
    Scalar,Scalar,Scalar, int, int >())
    .def("setAutotunerParams",&BondSwapGPU::setAutotunerParams)
    ;
    }
