# -*- coding: iso-8859-1 -*-
# Maintainer:

from hoomd_script import *
context.initialize()
import unittest
import os
import math

# tests for update.rescale_temp
class update_bond_swap_tests (unittest.TestCase):
    def setUp(self):
        print
        #make a system with two 5-mers
        self.snap = data.make_snapshot(N=10, box=data.boxdim(L=10),
                                   particle_types=['A'],
                                   bond_types=['polymer'],
                                   angle_types=['angle'],
                                   dihedral_types=['dihedral'],
                                   improper_types=[],
                                   dtype = 'float');
        self.s = init.read_snapshot(self.snap)
        sorter.set_params(grid=8);
        self.s.particles[0].position= (0.0, 0.0, 0.0)
        self.s.particles[1].position= (1.0, 0.0, 0.0)
        self.s.particles[2].position= (2.0, 0.0, 0.0)
        self.s.particles[3].position= (3.0, 0.0, 0.0)
        self.s.particles[4].position= (4.0, 0.0, 0.0)

        self.s.particles[5].position= (0.0, .9, 0.0)
        self.s.particles[6].position= (1.0, .9, 0.0)
        self.s.particles[7].position= (2.0, .9, 0.2)
        self.s.particles[8].position= (3.0, .9, -.2)
        self.s.particles[9].position= (4.0, .9, 0.0)

        for j in range(10):
            self.s.particles[j].type = 'A'

        #set up bonds, angles dihedrals
        self.s.bonds.add('polymer',0,1)
        self.s.bonds.add('polymer',1,2)
        self.s.bonds.add('polymer',2,3)
        self.s.bonds.add('polymer',3,4)
        self.s.bonds.add('polymer',5,6)
        self.s.bonds.add('polymer',6,7)
        self.s.bonds.add('polymer',7,8)
        self.s.bonds.add('polymer',8,9)
        self.s.angles.add('angle',0,1,2)
        self.s.angles.add('angle',1,2,3)
        self.s.angles.add('angle',2,3,4)
        self.s.angles.add('angle',5,6,7)
        self.s.angles.add('angle',6,7,8)
        self.s.angles.add('angle',7,8,9)
        self.s.dihedrals.add('dihedral',0,1,2,3)
        self.s.dihedrals.add('dihedral',1,2,3,4)
        self.s.dihedrals.add('dihedral',5,6,7,8)
        self.s.dihedrals.add('dihedral',6,7,8,9)

        #forces
        lj=pair.lj(r_cut=2.5)
        lj.pair_coeff.set('A','A',epsilon=.1, sigma=1.0)
        lj.set_params(mode="shift")
        harmonic = bond.harmonic()
        harmonic.bond_coeff.set('polymer',k=1.0, r0=1.0)
        harmang = angle.harmonic()
        harmang.set_coeff('angle',k=1.0,t0=2.0)
#        harmdih = dihedral.harmonic();
#        harmdih.set_coef('dihedral',k=0.25,d=1,n=2);

        all = group.all()
        integrate.mode_standard(dt=0.001)
        integrate.nvt(group=all,T=1.0,tau=0.5)

    # tests basic creation and running of updater
    def test(self):
        upd=update.bond_swap(period=25,fraction=.25,distSquared=1.9,Tnew=.5,seed=121);
        sysN = 10;
        newlist = []
        for i in range(sysN):
            newlist.append(1)
        upd.set_chain_positions(newlist)
        run(100)
        upd.print_stats()
        run(100)


    # test enable/disable
    def test_enable_disable(self):
        upd = update.bond_swap(period=25,phase=-1,fraction=0.5,distSquared=1.6,Tnew=1.0,seed=12321);
        upd.disable();
        self.assert_(not upd.enabled);
        upd.enable();
        self.assert_(upd.enabled);

    # test set_params
    def test_set_params(self):
        upd = update.bond_swap(period=25,phase=-1,fraction=0.5,distSquared=1.6,Tnew=1.0,seed=12321);
        upd.set_autotuner_params(enabled=True, period=20000);
        upd.set_exclusions(angle_exclude=True,dihedral_exclude=True);
        upd.set_safety_mode(safety=True);
        upd.set_mc_temp(new_temp=1.2);


    def tearDown(self):
        del self.s
        del self.snap
        init.reset();


if __name__ == '__main__':
    unittest.main(argv = ['test.py', '-v'])

